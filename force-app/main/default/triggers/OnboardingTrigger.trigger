/********************************************************************************************************************
@ Class:          OnboardingTrigger 
@ Version:        1.0
@ Author:         Mohan Swarna(mohan.swarna@cardinalhealth.com)
@ Purpose:        Sending Onboarding Request to MDG after approved.
---------------------------------------------------------------------------------------------------------------------
@ Change history: 09.23.2015 / Mohan Swarna / Created the class.
@ Change history: 09.05.2019 / Pavan Varma / Modified as per E6197(To avoid loop while updating the status)
@ Change history: 10.19.19   submit pending logic for callout
@ Change history: 03.12.19   syncOnboardingFields method is moved to Onboarding_Maintenance class
@ Change history: 03.16.20 / Accenture Jorge Mejia / added before update logic to call 'splitMaintenanceRequests'
@ Change history: 03.26.20 / Accenture Jorge Mejia / added before update logic to call 'cancelChildRecords'
*********************************************************************************************************************/
trigger OnboardingTrigger on Onboarding__c (Before Update,After Update) { 

    list <id> Submitted = new list <id> ();
    set <id> ApprovedIds = new set <id> ();
    list <onboarding__c> nonParMedSync = new list <Onboarding__c> ();
    list <Onboarding__c> parmedSync = new list <Onboarding__c> ();  
    string ParmedRectypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('ParMed Onboarding').getRecordTypeId();
    

    if( ActiveTriggers__c.getInstance('P-MOD TRIGGER CONTROL').OnboardingTrigger__c == false) {
        return;
    }    
    if(Trigger.isAfter && Trigger.isUpdate){
        for(Onboarding__c onbd : trigger.new){
            if (onbd.recordtypeId == ParmedRectypeId) {
                if(onbd.Approved__c != trigger.oldMap.get(onbd.Id).Approved__c && onbd.Approved__c == true){      
                    system.debug('** approved: ' + onbd.id);        
                    ApprovedIds.add(onbd.id);
                }                 
            } else {  // non-parmed records
                if (onbd.Onboarding_Status__c == 'Submit Pending' ) { 
                    system.debug('** submitted record ' + onbd);  
                    Submitted.add(onbd.id);
                }
            }   
        }        
        
        if (ApprovedIds.isempty() == false ) {
            system.debug('** approved callout' + ApprovedIds); 
            OnboardingTriggerHelper.callOnbdTrigger(ApprovedIds);
        }       
        if (Submitted.isempty() == false ) {
            system.debug('** submitted callout' + submitted);      
            OnboardingTriggerHelper.submitCallout(submitted);
        }
    } 
    
    if(Trigger.isBefore && Trigger.isUpdate){

        //methods to execute exactly once
        //if(CheckRecursive.runOnce()){

            //unchecks Act as... checkboxes
            OnboardingTriggerHelper.updtActAsChkBoxes(Trigger.new);

            //splits maintenance requests before submitting to MDG based on dependencies captured in Maintenance Request Types Metadata Types
            OnboardingTriggerHelper.splitMaintenanceRequests(Trigger.new);

            //propagates cancelled status to children maintenance records
            OnboardingTriggerHelper.cancelChildRecords(Trigger.new);
        //}
        
    }
}