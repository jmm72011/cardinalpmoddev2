/*------------------------------------------------------------------------------------------------------------------------------------------------------------
@ trigger:        AccountTrigger 
@ Version:        1.0
@ Author:         Mohan Swarna (mahan.swarna@accenture.com)
@ Purpose:        Deleting Account Assignment records when restricted flag is checked 
..............................................................................................................................................................
@ Change history: 06.12.2015/ MOHAN SWARNA/ Created the trigger.
------------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger AccountTrigger on Account (After Update) {
    
    final static string PMODTRIGGERCONTROL = 'P-MOD TRIGGER CONTROL';
    
    /*Trigger control - proceed if trigger switched ON*/
    ActiveTriggers__c activeTrig = ActiveTriggers__c.getInstance(PMODTRIGGERCONTROL);
    system.debug('**getInstance:' + activeTrig ); 
    if(ActiveTriggers__c.getInstance(PMODTRIGGERCONTROL).AccountTrigger__c) { 
        
        /* Checking Recursive trigger */
        if(CheckRecursive.runOnce()){          
            
            /*AFTER UPDATE Block - BEGIN*/
            /*Deleting Account Assignment records when restricted flag is checked on after update*/
            if(trigger.isUpdate && trigger.isAfter) {
                /* Calling helper class from trigger */
                AccountTriggerHelper.deleteAssignments(trigger.newMap.keySet());
                AccountTriggerHelper.deleteAssignmentsForInactiveAccounts(trigger.newMap.keySet());
                AccountTriggerHelper.legacyAccountRestriction(trigger.new);
               AccountTriggerHelper.emailNotificationToSalesRep(trigger.new, trigger.oldMap);
              // AccountTriggerHelper.updateOnboardingAddress(trigger.new, trigger.oldMap);
               
            }
            /*AFTER UPDATE Block - END*/
            
            /*AFTER INSERT Block - BEGIN*/
           /* if(trigger.isInsert && trigger.isAfter) {
                AccountTriggerHelper.legacyAccountRestriction(trigger.new);
            }*/
            /*AFTER INSERT Block - END*/
        } 
    }
}