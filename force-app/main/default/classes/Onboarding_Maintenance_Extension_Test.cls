/*************************************************************************************************************************************************************
@ Class:          Onboarding_Maintenance_Extension_Test
@ Version:        1.0
@ Author:         Abhirup Banik
@ Purpose:        Test Class for Onboarding button on Prospect Account and Maintenance button
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/
@isTest
public class Onboarding_Maintenance_Extension_Test{
    static testmethod void testMainExt(){
        insert new ActiveTriggers__c( name = 'P-MOD TRIGGER CONTROL', AccountTrigger__c = true);
        List<Account> accounts = TestDataFactory.createAccounts(2, 'CAH Customer');
        system.debug('** Accounts: ' + accounts);
        Id acctId = accounts[0].Id;
        Id acctId2 = accounts[1].Id;
        //List<Onboarding__c> Onboardings = TestDataFactory.createOnboardings(1,accounts[0].Id);
        List <Onboarding__c> OnboardData = new List<Onboarding__c>();
      
       Onboarding__c TestOnboard1 = new Onboarding__c(Name = 'Test Onbaord1',
             AccountName__c = acctId,
             RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId(),
             Servicing_Group__c = 'NATIONAL ACCOUNT',
             Account_Request_Type__c ='001; 006',
             Customer_Account_Group__c = 'ZPST',
             Onboarding_Status__c ='Draft',
             Payment_Type__c = 'ACH',
             Payment_Terms__c = 'Weekly Credit Card',
             Bill_To_Country__c = 'USA',
             Ship_To_Country__c = 'USA',
             Bill_To_Invoice_Method__c = 'Invoice - Print',
             Business_Unit__c ='PD',
             //Cardinal_business_unit__c = cardinalBURecs[0].id,//Added By Shreyashi
             EDI_Setup__c = true,
             Sticker_Perference_del__c='Price Sticker with Pricing',
             KYC__c='No',
             Has_Completed_the_Credit_Application__c= true,
             Initially_Purchase_Controlled_Substance__c= true,
             Rebate_Preferences__c='Credit',
             Lot_Expiration_Preferences__c='30',
             CSOS__c= true,
             Approved__c = false,
             Priority__c='Normal',
             Bill_To_License_Name__c='tesrty',
             Bill_To_Telephone__c='123654789',
             Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
             Ship_To_Email_Address__c='test@tset.com',
             Ship_To_Fax_Number__c='515225-3170',
             Ship_To_Phone_Number__c='515225-3170',
             Bill_To_Zip_Code__c='55440',
             Bill_To_City__c='MINNEAPOLIS',
             Bill_To_State__c='MN',
             Bill_To_Address1__c='PO BOX 9493',
             Ship_To_Zip_Code__c='45678',
             Ship_To_City__c='city',
             Ship_To_State__c='IA',
             Request_Type__c='Full Setup',
             Sync_Fields__c = false,
             Credit_Limit__c=234.00,
             Customer_Classification__c='sss',
             Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
             Estimated_First_Purchase_Date__c = system.today(),
             Is_this_new_business_to_Cardinal_Health__c ='Yes,',
             //Class_of_Trade_Level_1__c = '01 - HOSPITAL;',
             Requested_Credit_Limit__c =123.00);
        //Allow_Duplicate_Purchase_Orders__c='Yes');
        OnboardData.add(TestOnboard1);
        
        Onboarding__c TestOnboard2 = new Onboarding__c(Name = 'Test Onbaord2',
                AccountName__c = acctId,
                RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId(),                                      
                 Servicing_Group__c = 'NATIONAL ACCOUNT',
                 Account_Request_Type__c ='001',
                 Customer_Account_Group__c = 'ZPST',
                 Onboarding_Status__c ='In Progress',
                 Payment_Type__c = 'ACH',
                 Payment_Terms__c = 'Weekly Credit Card',
                 Bill_To_Country__c = 'USA',
                 Ship_To_Country__c = 'USA',
                 Bill_To_Invoice_Method__c = 'Invoice - Print',
                 Business_Unit__c ='PD',
                 //Cardinal_business_unit__c = cardinalBURecs[0].id,//Added By Shreyashi
                 EDI_Setup__c = true,
                 Sticker_Perference_del__c='Price Sticker with Pricing',
                 KYC__c='No',
                 Has_Completed_the_Credit_Application__c= true,
                 Initially_Purchase_Controlled_Substance__c= true,
                 Rebate_Preferences__c='Credit',
                 Lot_Expiration_Preferences__c='30',
                 CSOS__c= true,
                 Approved__c = false,
                 Priority__c='Normal',
                 Bill_To_License_Name__c='tesrty',
                 Bill_To_Telephone__c='123654789',
                 Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
                 Ship_To_Email_Address__c='test@tset.com',
                 Ship_To_Fax_Number__c='515225-3170',
                 Ship_To_Phone_Number__c='515225-3170',
                 Bill_To_Zip_Code__c='55440',
                 Bill_To_City__c='MINNEAPOLIS',
                 Bill_To_State__c='MN',
                 Bill_To_Address1__c='PO BOX 9493',
                 Ship_To_Zip_Code__c='45678',
                 Ship_To_City__c='city',
                 Ship_To_State__c='IA',
                 Request_Type__c='Full Setup',
                 Sync_Fields__c = false,
                 Credit_Limit__c=234.00,
                 Customer_Classification__c='sss',
                 Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
                 Estimated_First_Purchase_Date__c = system.today(),
                 Is_this_new_business_to_Cardinal_Health__c ='Yes,',
                 //Class_of_Trade_Level_1__c = '01 - HOSPITAL;',
                 Requested_Credit_Limit__c =123.00);                
        OnboardData.add(TestOnboard2);
        
        Onboarding__c TestOnboard3 = new Onboarding__c(Name = 'Test Onbaord3',
                 AccountName__c = acctId,
                 RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId(),
                 Servicing_Group__c = 'NATIONAL ACCOUNT',
                 Account_Request_Type__c ='002',
                 Customer_Account_Group__c = 'ZPST',
                 Onboarding_Status__c ='Sent',
                 Payment_Type__c = 'ACH',
                 Payment_Terms__c = 'Weekly Credit Card',
                 Bill_To_Country__c = 'USA',
                 Ship_To_Country__c = 'USA',
                 Bill_To_Invoice_Method__c = 'Invoice - Print',
                 Business_Unit__c =null,
                 //Cardinal_business_unit__c = cardinalBURecs[0].id,//Added By Shreyashi
                 EDI_Setup__c = true,
                 Sticker_Perference_del__c='Price Sticker with Pricing',
                 KYC__c='No',
                 Has_Completed_the_Credit_Application__c= true,
                 Initially_Purchase_Controlled_Substance__c= true,
                 Rebate_Preferences__c='Credit',
                 Lot_Expiration_Preferences__c='30',
                 CSOS__c= true,
                 Approved__c = false,
                 Priority__c='Normal',
                 Bill_To_License_Name__c='tesrty',
                 Bill_To_Telephone__c='123654789',
                 Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
                 Ship_To_Email_Address__c='test@tset.com',
                 Ship_To_Fax_Number__c='515225-3170',
                 Ship_To_Phone_Number__c='515225-3170',
                 Bill_To_Zip_Code__c='55440',
                 Bill_To_City__c='MINNEAPOLIS',
                 Bill_To_State__c='MN',
                 Bill_To_Address1__c='PO BOX 9493',
                 Ship_To_Zip_Code__c='45678',
                 Ship_To_City__c='city',
                 Ship_To_State__c='IA',
                 Request_Type__c='Full Setup',
                 Sync_Fields__c = false,
                 Credit_Limit__c=234.00,
                 Customer_Classification__c='sss',
                 Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
                 Estimated_First_Purchase_Date__c = system.today(),
                 Is_this_new_business_to_Cardinal_Health__c ='Yes,',
                 //Class_of_Trade_Level_1__c = '01 - HOSPITAL;',
                 Requested_Credit_Limit__c =123.00);                
        OnboardData.add(TestOnboard3);
        
        insert OnboardData;       
        
        Onboarding__c TestOnboard4 = new Onboarding__c(
                 Name = 'Test Onbaord4',
                RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId(),                              
                 AccountName__c = acctId2,
                 Servicing_Group__c = 'NATIONAL ACCOUNT',
                 Account_Request_Type__c ='006',
                 Customer_Account_Group__c = 'ZPST',
                 Onboarding_Status__c ='Error',
                 Payment_Type__c = 'ACH',
                 Payment_Terms__c = 'Weekly Credit Card',
                 Bill_To_Country__c = 'USA',
                 Ship_To_Country__c = 'USA',
                 Bill_To_Invoice_Method__c = 'Invoice - Print',
                 Business_Unit__c ='PD',
                 //Cardinal_business_unit__c = cardinalBURecs[0].id,//Added By Shreyashi
                 EDI_Setup__c = false,
                 Sticker_Perference_del__c='Price Sticker with Pricing',
                 KYC__c='No',
                 Has_Completed_the_Credit_Application__c= true,
                 Initially_Purchase_Controlled_Substance__c= true,
                 Rebate_Preferences__c='Credit',
                 Lot_Expiration_Preferences__c='30',
                 CSOS__c= true,
                 Approved__c = false,
                 Priority__c='Normal',
                 Bill_To_License_Name__c='tesrty',
                 Bill_To_Telephone__c='123654789',
                 Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
                 Ship_To_Email_Address__c='test@tset.com',
                 Ship_To_Fax_Number__c='515225-3170',
                 Ship_To_Phone_Number__c='515225-3170',
                 Bill_To_Zip_Code__c='55440',
                 Bill_To_City__c='MINNEAPOLIS',
                 Bill_To_State__c='MN',
                 Bill_To_Address1__c='PO BOX 9493',
                 Ship_To_Zip_Code__c='45678',
                 Ship_To_City__c='city',
                 Ship_To_State__c='IA',
                 Request_Type__c='Full Setup',
                 Sync_Fields__c = false,
                 Credit_Limit__c=234.00,
                 Customer_Classification__c='sss',
                 Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
                 Estimated_First_Purchase_Date__c = system.today(),
                 Is_this_new_business_to_Cardinal_Health__c ='Yes,',
                 //Class_of_Trade_Level_1__c = '01 - HOSPITAL;',
                 Requested_Credit_Limit__c =123.00);
        insert TestOnboard4;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(OnboardData[0]);
        Onboarding_Maintenance_Extension testMainExt = new Onboarding_Maintenance_Extension(sc);
        
        PageReference pageRef = Page.Onboarding_Maintenance_Edit_Page;
        pageRef.getParameters().put('id', String.valueOf(OnboardData[0].id));
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(TestOnboard4);
        Onboarding_Maintenance_Extension testMainExt1 = new Onboarding_Maintenance_Extension(sc1);
        
        PageReference pageRef1 = Page.Onboarding_Maintenance_Edit_Page;
        pageRef.getParameters().put('id', String.valueOf(TestOnboard4.id));
        Test.setCurrentPage(pageRef1);
        
        ApexPages.StandardController sc2 = new ApexPages.StandardController(TestOnboard3);
        Onboarding_Maintenance_Extension testMainExt2 = new Onboarding_Maintenance_Extension(sc2);
        
        PageReference pageRef2 = Page.Onboarding_Maintenance_Edit_Page;
        pageRef.getParameters().put('id','');
        Test.setCurrentPage(pageRef2);
        
        
        testMainExt.save();
        testMainExt.redirectionEditPage();
        testMainExt1.save();
        testMainExt1.redirectionEditPage();
        testMainExt1.restrictSaveOnStatus('Sent');
        testMainExt2.save();
    }  
}