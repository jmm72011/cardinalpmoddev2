/*************************************************************************************************************************************************************
@ Class:          OAuthConnector
@ Version:        1.0
@ Author:         Vasantha Nelahonne
@ Purpose:        Common Connector class to get Token from Apigee. Every services must use this connector to get a token and refresh token before callout
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/
global with sharing class OAuthConnector {

    public String apigeeResponse {set; get;}
    public String errorMessage {set; get;}
    final static String OAUTH_CONNECTOR = 'Apigee OAuth Connector';
    final static String AUTHORIZATION = 'Authorization';
    final static String OAUTH_BODY = 'grant_type=client_credentials';
    final static String METHOD = 'POST';
    final static String MSG_TIMESTAMP = 'x-cah-msg-timestamp';    //This is a dynamic time to represent callout initiated time.
    final static String ACCESS_TOKEN='access_token';
 
    public String getOAuthToken() {

        String OAuthToken = null;

        try{
            HttpRequest hreq = new HttpRequest();
            hreq.setEndpoint(Endpoints__c.getInstance(OAUTH_CONNECTOR).End_Point_URL__c);
            hreq.setHeader(AUTHORIZATION, Endpoints__c.getInstance(OAUTH_CONNECTOR).Authorization_Header__c);
            hreq.setHeader(MSG_TIMESTAMP, DateUtil.todayDateTimeMS_UTC());
            hreq.setMethod(METHOD);
            hreq.setBody(OAUTH_BODY);

            Http objHttp = new Http();
            HTTPResponse objRes = objHttp.send(hreq);
            system.debug('HTTP Response :'+objRes.getStatusCode());
            System.debug('HTTP Response :'+ objRes.getBody());
            JSONParser jsonparse = JSON.createParser(objRes.getBody());

            system.debug('jsonparse'+jsonparse);
            while (jsonparse.nextToken() != null)
            {
                if (jsonparse.getCurrentToken().equals(JSONToken.FIELD_NAME) && jsonparse.getText().equalsIgnoreCase(ACCESS_TOKEN))
                {
                    jsonparse.nextToken();
                    OAuthToken = jsonparse.getText();
                    System.Debug('OAuthToken : ' + OAuthToken);
                }
            }
         }
         catch(Exception ex)
         {  errorMessage = ex.getMessage();
            System.debug('OAuth Callout Exception :' + ex.getStackTraceString());
            System.debug('OAuth Callout Exception :' + ex.getMessage());
         }

         return OAuthToken;
    }

    public static HttpRequest addOAuthToken(HTTPRequest request)
    {
        try{
        OAuthConnector oAuth = new OAuthConnector();
        String OAuthToken = oAuth.getOAuthToken(); // Generate new access token before calling bellow API
        request.setHeader(AUTHORIZATION, 'Bearer ' + OAuthToken);
         return request;
        }catch(Exception ex){            
            System.debug('OAuth Callout Exception :' + ex.getStackTraceString());
            System.debug('OAuth Callout Exception :' + ex.getMessage());
            return null;
           }
        
       
    }
 }