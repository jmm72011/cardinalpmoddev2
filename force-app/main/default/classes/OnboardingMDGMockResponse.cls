@isTest(seeAllData = False)
global class OnboardingMDGMockResponse implements WebServiceMock {
            
    global String result; //Created to set the value for one of the request variable 'Message'.
    global String type_x; //Created to set the value for one of the request variable 'type_x'.    
    global OnboardingMDGMockResponse (String result, String type_x){
        this.result = result;
        this.type_x = type_x;
    }
    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) { 
    
        OnboardingMDGWebService.Y_MDG_CUS_SFDC_ONBOARDResponse_element respElement = new  OnboardingMDGWebService.Y_MDG_CUS_SFDC_ONBOARDResponse_element ();
        respElement.ET_MESSAGE = new OnboardingMDGWebService.BAPIRET2_T();
        respElement.ET_MESSAGE.item = new List<OnboardingMDGWebService.BAPIRET2>();
        OnboardingMDGWebService.BAPIRET2 bapItem = new OnboardingMDGWebService.BAPIRET2();            
        bapItem.MESSAGE = this.result;
        bapItem.TYPE_x = this.type_x;
        respElement.ET_MESSAGE.item.add(bapItem);
        response.put('response_x', respElement); 
   }

}