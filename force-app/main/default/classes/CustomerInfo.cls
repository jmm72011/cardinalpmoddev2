/*************************************************************************************************************************************************************
@ Class:          CustomerInfo
@ Version:        1.0
@ Author:         Bibekini Behera
@ Purpose:        Customer Service Data Structure.
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/

global with sharing Class CustomerInfo
{
        //REQUEST DATA
        public String szCustomerName;
        public String szCustomerNumber;
        public String szSODCDV;
        public String startdate;
        public String enddate;
        public String accountId;
        public String businessUnit;
        public String nextTierAmount;
        public List<String> businessUnits;

        //Service Controlling fields
        public boolean withEmergency;
        public boolean withCOGS;
        public boolean withRebate;
        
        //Error Tracking
        public List<ServiceMessage>     message;
        public List<ServiceMessage>     errors;

        //COMMON FIELDS
        public String requestLabel='';//added by bibekini
        public String requestLabel2=null;
        
        // Rebates Request Variables
        public String agreementNumber;
        public Boolean includeAllPartnerAccounts;
        public String fromDate;
        public String toDate; 
        public String requestedDataType;
        public Boolean isAllAgreement;
        public String productCategory;
        public Boolean isRebatePaid;
        public Boolean isRebateMatrix;
        public Boolean isAccountBU;
        public Boolean isQuoteUI;
        public Boolean isPriceDesk;
        public Decimal totalMtdSales;
        public String genericsMRrate;
        public String genericsMRName;
        public String genericsMRUnit;
        public String avgRebatePaid;

        //RESPONSE DATA

        //CUSTOMER FIELDS
        public Account acc{get;set;}
        public Boolean isLicenseabouttoExpired {get;set;}
        public Boolean isLicenseExpired {get;set;}
        public Integer noOfDaysRemaining {get;set;}
        public Boolean isOrderExistToday {get;set;}
        public SAP_Sales_Area__c accSalesArea {get;set;}
    
        Public List<Security_Matrix__c> securityRecord;
        Public List<Territory2> accountLevelAccess;
    
    
    
 /********* Service Monitoring **********/
  public DateTime AppStartTime;//APP CALLOUT START(THIS TIME SENT FROM APP) 
  public DateTime ServiceStartTime;//CALLOUT START 
  public DateTime SFStartTime =System.now();//REMOTE ACTION START
  public DateTime ServiceEndTime;//CALLBACK START 
  public DateTime SFEndTime;//REMAP ENDTIME 
  Public Boolean ServiceMonitorLog =false;
  Public Boolean GlobalServiceMonitorLog=true;
  public String ServiceName;
  //Public Boolean GlobalServiceMonitorLog=Boolean.valueOf(label.GlobalServiceMonitorLog);
  /**************************************/
        

        //EMERGENCY FIELDS
        public Integer emergencydeliveryCount {get;set;}//added by bibekini


        public String             SalesOrg;
        public String             distChannel ;
        public String             division ;
        public String             classofTrade;//request field

        //Rebate FIELDS
        public with sharing class RebatePaid
        {
            // Rebates Paid Response Variables
            public String custNumber ;
            public String agreementNumber ;
            public String salesOrg ;
            public String distribChnl ;
            public String division ;
            public String agreementEndDate ;
            public String calculationRun ;
            public String lastRebatePaid ;
            public String prodCategory ;
            public String mRRebatePaid ;
            public String lRPaidCurrency ;
            public String lastTotNetSale ;
            public String rebateRateUnit ;
            public String lastRebateRate ;
            public String rebatePaidDate ;
            public String payer ;
            public String payerName ;
            public String driverRebatePaid ;
            public String driverDescription ;
            public String mRRebPaidGeneric ;
            public String filterAppliedTo ;
            public String filterAppliedDesc ;
            public String frequency ;
            public String id;
            public Decimal mtdSalesTotal;
            public String avgRebPaid;
            
         
        }
        public List<RebatePaid>  rebatePaidList;
        
        public with sharing class RebateMatrix{
            // Rebates Matrix Response Variables
            public String customerNumber ; 
            public String agreementNumbr ;  
            public String tierLevel ; 
            public String dimension1 ; 
            public String dim1Desc ; 
            public String dimension2 ; 
            public String dim2Desc ; 
            public String dimesion3 ; 
            public String dim3Desc ; 
            public String rebateMatrixRate ; 
            public String rebateRateUnitForMatrix ; 
            public String driverRebatePaidForMatrix ; 
            public String driverDescriptionForMatrix ; 
            public String filterAppliedToForMatrix ; 
            public String rebateType ; 
            public String productCategory ; 
            public String rebateRate ; 
            public String unit ; 
            public String salesOrg ;
            public String distribChnl ;
            public String division ;
            public String appliedTodesc ; 
            public String paymentTypeDesc ; 
            public String currentTier;
            public String agreementEndDateForMatrix ; 
            public String id;
        }
        public List<RebateMatrix>   rebateMatrixList;
        public List<RebateMatrix>   rebateCurrMatrixList;
        
        //COGS FIELDS
        public with sharing class COGS
        {
            public String             soldTo ;
            public String             validTo;//check with ui if they need date or string
            public String             validFrom;//check with ui if they need date or string
            public String             priority;
            public String             factor;
            public String             additionalMarkup;
            public String             clsofTrade;//response field
            public String             description;//response field
        }

        public CrossBU      crossBURecord;
        public List<Cogs>   cogsList;

        //REBATE FIELDS

        //crossBuMatrix

        //Inner calss for cross BU Matrix
        global with sharing class CrossBU {
            public List<String> productSearchBUList;
            public List<String> webSrvcBUList;
            public List<String> orderBUList;
            public List<String> accountAccessBUList;
            public String customerPrimaryBU;
            public String territoryBU;
            public List<String> multipleBUList;
            public boolean isMultipleBU;

            /* For future use
            public List<String> contactAccessBUList;
            public List<String> opportunityAccessBUList;
            public List<String> caseAccessBUList;*/
        }

        public CustomerInfo(){
                acc = new Account();
                isLicenseabouttoExpired = false;
                isLicenseExpired = false;
                noOfDaysRemaining = 0;
                isOrderExistToday = false;
                accSalesArea = new SAP_Sales_Area__c();
                emergencydeliveryCount=0;
                if(!String.isBlank(szSODCDV)){
                    getsodcdv(szSODCDV);
                }
        }
    
/*********************************************************************************************************************************************************
@ Constructor:    getsodcdv
@ Version:        1.0
@ Author:         Bibekini Behera
@ Purpose:        Add message to cart
----------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**********************************************************************************************************************************************************/   
        public void getsodcdv(String sodcdv)
        {
            try{
            	this.SalesOrg=sodcdv.substring(0,4);
                this.distChannel=sodcdv.substring(4,6);
                this.division=sodcdv.substring(6,8); 
            }
            catch(Exception e ){
                System.Debug('CustomerInfo Exception: getsodcdv ' + e.getMessage() );
                System.Debug('CustomerInfo Exception: getsodcdv ' + e.getStackTraceString() );
        	}
        }
    	
/*********************************************************************************************************************************************************
@ Constructor:    addMessage
@ Version:        1.0
@ Author:         Bibekini Behera
@ Purpose:        Add message to cart
----------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**********************************************************************************************************************************************************/   
        public void addMessage(String eMessage) {
            try{
               this.message = new List<ServiceMessage>();
               this.message.add(ServiceMessage.addMessage('200',eMessage,'','')); 
            }
            catch(Exception e ){
                System.Debug('CustomerInfo Exception: addMessage ' + e.getMessage() );
                System.Debug('CustomerInfo Exception: addMessage ' + e.getStackTraceString() );
        	}
        }
    }