/*************************************************************************************************************************************************************
@ Class:          CogsServiceData
@ Version:        1.0
@ Author:         Bibekini Behera
@ Purpose:        Data Class for COGS Service
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history: 08.06.2019 / Bibekini Behera / Created the class.

**************************************************************************************************************************************************************/
public with sharing class COGSServiceData  extends RestServiceData
{
    public COGSData d;
	/*************************************************************************************************************************************************************
    @ Class:          COGS
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        
    --------------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Change history:
    **************************************************************************************************************************************************************/ 
    public with sharing class COGS{

      public  String    Shipto {get;set;}
      public  String    SalesOrg {get;set;}
      public  String    DistrChannel {get;set;}
      public  String    Division {get;set;}
      public  String    Soldto {get;set;}
      public  String    BuyingGroup {get;set;}
      public  String    ToDate {get;set;}
      public  String    FromDate {get;set;}
      public  String    Hiercy {get;set;}
      public  String    Factr {get;set;}
      public  String    Mrkup {get;set;}
      public  String    Description {get;set;}
    }
	/*************************************************************************************************************************************************************
    @ Class:          COGSData
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        
    --------------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Change history:
    **************************************************************************************************************************************************************/ 
    public with sharing class COGSData{
        public List<COGS> results = new List<COGS>();
    }



    /*********************************************************************************************************************************************************
    @ Method:         retrieveJSONResponse
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        Retrieve  data from the JSON Response.    Required Override virtual method and implemented for all POST Calls
                      Required for both POST Callout and GET Callout
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:     Input App Request
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        Request Data a JSONGenerator
    **********************************************************************************************************************************************************/
    public virtual override Object retrieveJSONResponse(Object response)
    {
        try{
        if ( response != null )
        {
            HTTPResponse jsonResponse = (HTTPResponse) response;
            system.debug('*******'+jsonResponse.getBody());
            String restResponse=jsonResponse.getBody();
            system.debug('response body'+restResponse);

            COGSServiceData cogsResponse=(COGSServiceData) System.JSON.deserialize(restResponse, COGSServiceData.class);
            System.Debug('Deserialized restResponse Data from COGSServiceData : ' + cogsResponse);

            return cogsResponse;
        }
            return ServiceMessage.sendErrorMessage('Response is coming blank');
        }catch (Exception e)
            {
                
                System.Debug('retrieveJSONResponse: Exception Occured: ' + e.getMessage());
                System.Debug('retrieveJSONResponse: Exception Occured: ' + e.getStackTraceString());
                ServiceMessage errorMessage = ServiceMessage.sendErrorMessage( 'Last Purchase retrieveJSONResponse Exception : ' + e.getMessage(), '', 'Retrieve LastPurchase' );
            	return errorMessage;
            }
        
    }


    /*********************************************************************************************************************************************************
    @ Method:         generateJSONRequest
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        Creates JSON Request data.     Optional Override virtual method and implemented for all POST Calls
                      Required only for POST Callout.  NOT REQUIRED for GET Callout
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:     Input App Request
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        Request Data a JSONGenerator
    **********************************************************************************************************************************************************/
   /*public virtual override Object generateJSONRequest(Object request)
     {
       //This method is NOT Required. for POST Method no need for creating payload
     }*/
}