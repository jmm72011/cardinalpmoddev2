/*************************************************************************************************************************************************************
@ Class:          Onboarding_Maintenance_Extension
@ Version:        1.0
@ Author:         Abhirup Banik
@ Purpose:        Extension class for Onboarding_Maintenance VF page
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/
public with sharing class Onboarding_Maintenance_Extension{
    
    private final Onboarding__c onb;
    public boolean restrict{get;set;}
    final static String CLASS_NAME           = 'Onboarding_Maintenance_Extension';
    final static String  ALERT_MESSAGE_DML           = 'DML Exception';
    private static final String METHOD_NAME          = 'save';
    private static final String LOG_TYPE             = 'ERROR';
    private static final String ALERT_TYPE           = 'APPLICATION_ALERTS';
    final static String  ALERT_MESSAGE_EXE           = 'DML Exception';        
    private static final String SEVERITY_CRITICAL    = 'CRITICAL';
    private static final String SEVERITY_HIGH        = 'HIGH';
    private final static String ONBOARDING_MAINTENANCE_EDIT_PAGE_URL= '/apex/Onboarding_Maintenance_Edit_Page?id=';
    private final static String HTTPS= 'https://';
    private final static String SLASH = '/';
    private final static String CONST_DRAFT = 'Draft';
    private final static String CONST_SUB_PEND = 'Submit Pending';
    private final static String CONST_SENT = 'Sent';
    private final static String CONST_IN_PROG = 'In Progress';
    private final static String CONST_ERR = 'Error';
    
    public Onboarding_Maintenance_Extension(ApexPages.StandardController stdController) {
        this.onb = (Onboarding__c)stdController.getRecord();
        restrictSaveOnStatus(onb.Onboarding_Status__c);        
    }
    
    public PageReference save(){
        
        String hostname = URL.getSalesforceBaseUrl().getHost();
        System.debug('Host>>'+hostname );
        Integer matchFound = 0;
        PageReference pg; //return url
        ApexPages.message existingOnbReqMsg = new ApexPages.message(ApexPages.severity.INFO,'An active Onboarding request already exists for this account and request type, cannot save.'); 
        
        List<Onboarding__c> onbList = [Select Id,Account_Request_Type__c
                                       ,Onboarding_Status__c, Business_unit__c 
                                       FROM Onboarding__c where AccountName__c = :onb.AccountName__c LIMIT 50000];//onb records under account at least 1 record
        
        for(Onboarding__c vonb : onbList){            
            Set<String> ReqTypeList;
            if(vonb.Account_Request_Type__c != null && vonb.Account_Request_Type__c.contains(';')){
                ReqTypeList = new Set<String>(vonb.Account_Request_Type__c.split(';')); //splitting multiple values of 'Acc Request type' of all records
            }else{
                ReqTypeList = new Set<String>();
                if(vonb.Account_Request_Type__c != null)
                    ReqTypeList.add(vonb.Account_Request_Type__c); //adding splited single value of 'Acc Request type'
            }
            if(ReqTypeMatchFound(ReqTypeList,onb.Account_Request_Type__c) && String.isNotBlank(onb.Account_Request_Type__c) && onbList.size()>1 
               && (onb.Id != vonb.Id)//bypassing the check for same record allowing to update
               && (CONST_DRAFT.equalsIgnoreCase(vonb.Onboarding_Status__c) ||
                   CONST_SUB_PEND.equalsIgnoreCase(vonb.Onboarding_Status__c) ||
                   CONST_SENT.equalsIgnoreCase(vonb.Onboarding_Status__c) ||
                   CONST_IN_PROG.equalsIgnoreCase(vonb.Onboarding_Status__c) ||
                   CONST_ERR.equalsIgnoreCase(vonb.Onboarding_Status__c))){                       
                       ApexPages.addmessage(existingOnbReqMsg);                        
                       matchFound++;
                       pg=null;
                   }            
        }
        if(onb.Account_Request_Type__c == null){
            //Validating Req Type to select at leat one value
            matchFound = -1;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Select at least one Account Request Type'));
        }
        if(onb.Business_unit__c == null){
            //Validating Business Unit to select a value             
            matchFound = -1;           
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Select a Business Unit'));
        }
            
        if(matchFound==0){  
            //check Hide Request Type field after first save
            if(!onb.Hide_Request_Type__c)
                onb.Hide_Request_Type__c = true;  
                         
            try{
                update onb;
                pg = new PageReference(HTTPS+hostname+SLASH+onb.Id);                
            }catch(Exception e){
                
                ApexLogger.sendAlert(e.getMessage(),CLASS_NAME ,METHOD_NAME ,LOG_TYPE ,ALERT_TYPE ,SEVERITY_HIGH);
            }
        }

        
        
        return pg;
        
    }
    //checking the given request type onb rec already exists in Acc or not
    private Boolean ReqTypeMatchFound(Set<String> ReqTypeList, String ReqType){       
        Set<String> givenReqType;
        Boolean bmatchFound;
        
        if(ReqType!=null && ReqType.contains(';')){
            //spliting, if multiple Requests types given 
            givenReqType = new Set<String>(ReqType.split(';')); 
        }else{
            givenReqType = new Set<String>();
            if(ReqType!=null){   
                givenReqType.add(ReqType);
            }
        }
        Set<String> commonVal = new Set<String>(ReqTypeList);
        commonVal.retainAll(givenReqType); //retaining the given Request types if exists already
        System.debug('Common Value>>>>'+commonVal); 
        if(commonVal.size()>0){
            bmatchFound = true;
        }else
            bmatchFound = false;
        
        return bmatchFound;
    }
    //disable the Save button on Status not in Draft
    private void restrictSaveOnStatus(String status){
        System.debug('Status>>>'+status);        
        if(status != 'Draft' && status != 'Error'){
            restrict = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Changes not allowed, record not in Draft Status.'));
        }      
        else
            restrict = false;                    
    }
    public PageReference redirectionEditPage(){
        // if record type is Maintenance, redirect to VF page, else standard page   
        PageReference pg;
        try{
            String hostname = URL.getSalesforceBaseUrl().getHost();
            
            Id MaintRecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId();
            if(onb.RecordTypeId == MaintRecordTypeId){
                pg  = new PageReference(ONBOARDING_MAINTENANCE_EDIT_PAGE_URL+onb.Id);
            }else{
                pg  = new PageReference(HTTPS+hostname+SLASH+onb.Id);
            }
            return pg;    
        }catch(Exception e){
            System.debug(e.getMessage());
            return null;
        }
    }
}