/*************************************************************************************************************************************************************
@ Class:          Onboarding_EditPage_Redirection_Extension
@ Version:        1.0
@ Author:         Abhirup Banik
@ Purpose:        Extension class for Onboarding_EditPage_Redirection VF page
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/
public with sharing class Onboarding_EditPage_Redirection_Ext{

 private ApexPages.StandardController controller;
public String retURL {get; set;}
public String saveNewURL {get; set;}
public String rType {get; set;}
public String cancelURL {get; set;}
public String ent {get; set;}
public String confirmationToken {get; set;}
public Onboarding__c onb;
private final static String MAINTENANCE = 'Maintenance';
private final static String ONBOARDING_MAINTENANCE_EDIT_PAGE_URL= '/apex/Onboarding_Maintenance_Edit_Page?id=';
private final static String SLASH = '/';
private final static String SLASHE= '/e';
private final static String ID = 'id';
private final static String RET_URL = 'retURL';
private final static String RECORDTYPE = 'RecordType';
private final static String CANCELURL2 = 'cancelURL';
private final static String ENT2 = 'ent';
private final static String CONFIRMATIONTOKEN2= '_CONFIRMATIONTOKEN';
private final static String SAVE_NEW_URL = 'save_new_url';
private final static String NOOVERRIDE = 'nooverride';
private final static String CLASS_NAME                                             = 'Onboarding_EditPage_Redirection_Ext';
private final static String METHOD_NAME                                            = 'redirectionEditPage';
private final static String LOG_TYPE                                               = 'ERROR';
private final static String ALERT_TYPE                                             = 'APPLICATION_ALERTS';
private final static String SEVERITY_CRITICAL                                      = 'CRITICAL';
private String keyId;

public Onboarding_EditPage_Redirection_Ext(ApexPages.StandardController controller) {

    this.controller = controller;
    this.onb = (Onboarding__c)controller.getRecord();
    retURL = ApexPages.currentPage().getParameters().get('retURL');    
    cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
    ent = ApexPages.currentPage().getParameters().get('ent');
    //confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
    saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
    keyId = Schema.SObjectType.Onboarding__c.getKeyPrefix();      

}

public PageReference redirectionEditPage() {
    try{
        PageReference returnURL;
        
        // Redirect if Record Type corresponds to custom VisualForce page
    
        IF(onb.recordTypeId== getRecordTypeId(MAINTENANCE)) {
    
            returnURL = new PageReference(ONBOARDING_MAINTENANCE_EDIT_PAGE_URL+onb.Id);
    
        }
    
        ELSE {        
            returnURL = new PageReference(SLASH+keyId+SLASHE);
    
        }
        returnURL.getParameters().put(ID, onb.id);
        returnURL.getParameters().put(RET_URL, retURL);
        returnURL.getParameters().put(RECORDTYPE, rType);
        returnURL.getParameters().put(CANCELURL2 , cancelURL);
        returnURL.getParameters().put(ENT2, ent);
        returnURL.getParameters().put(CONFIRMATIONTOKEN2, confirmationToken);
        returnURL.getParameters().put(SAVE_NEW_URL, saveNewURL);
        returnURL.getParameters().put(NOOVERRIDE, '1');
    
        returnURL.setRedirect(true);
        return returnURL;
       }catch(exception e)
       {
            system.debug('** exception caught: ' + e.getmessage());
            list <exception>  liste = new list <Exception> ();
            liste.add(e);
            ApexLogger.sendAlert(liste, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_CRITICAL );
            return null;
        }
    }
    private static Id getRecordTypeId(String recordtypeName){        
        return Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get(recordtypeName).getRecordTypeId();        
    }
 
}