/*************************************************************************************************************************************************************
@ Class:          PMod2_CostofGoodsView_EXT
@ Version:        1.0
@ Author:         Pavan Varma (pavan.varma@accenture.com)
@ Purpose:        Trigger Helper Class for Business Unit Contact object
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history: 13.08.2019 / Pavan Varma / Created the class.
**************************************************************************************************************************************************************/
public with sharing class CostofGoodsView_EXT {

    //public boolean showCOGButton{get;set;}
    public String userBu { get; set;}
    public String accountBusinessBu { get; set;}
    public String accountRecordType { get; set;}
    public String currentRecordId;
    public String abuCustomerNumber { get; set;}
    public String abuSalesOrg { get; set;}
    public String abuDistributionChannel { get; set;}
    public String abuDivision { get; set;}
    public String abuClassofTrade;
    public String abuBuyingGroup { get; set;}
    public Account acc;
    public SAP_Sales_Area__c abu;
    public Cardinal_Business_Unit__c carBu;
    public String abuDate{get;set;}
      /******************************************************************************************************************************************************
    Constructor:      CostofGoodsView_EXT
    @ Version:        1.0
    @ Author:         Pavan Varma
    @ Purpose:        To send the data in Vf Page for Cogs Button
    **********************************************************************************************************************************************************/
    public CostofGoodsView_EXT(ApexPages.StandardController stdCont) {
        User usr = [Select Id, Business_Unit__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        if(currentRecordId  != null){
            acc = [SELECT Id,Name, RecordType.Name, RecordTypeId FROM Account WHERE Id in (Select Account__c from SAP_Sales_Area__c where Id=:currentRecordId) LIMIT 50000];
            carBu=[SELECT Id,Name FROM Cardinal_Business_Unit__c WHERE Id in (Select Business_Unit__c from SAP_Sales_Area__c where Id=:currentRecordId) LIMIT 50000];
            abu = [Select Account__c,Customer_Account_Number__c,SAP_Sales_Organization__c,SAP_Distribution_Channel__c,SAP_Division__c,Class_of_Trade_Code__c,Buying_Group__c from SAP_Sales_Area__c where Id =:currentRecordId LIMIT 50000];
        }
        userBu = usr.Business_Unit__c;
        accountBusinessBu = carBu!= null ? carBu.Name : '';
        accountRecordType =acc!=null ? acc.RecordType.Name : '';
        abuCustomerNumber = abu !=null ? abu.Customer_Account_Number__c : '';
        abuSalesOrg = abu !=null ? abu.SAP_Sales_Organization__c : '';
        abuDistributionChannel = abu !=null ? abu.SAP_Distribution_Channel__c : '';
        abuDivision = abu !=null ? abu.SAP_Division__c : '';
        abuBuyingGroup = abu !=null ? abu.Buying_Group__c : '';
        abuClassofTrade = abu !=null ? abu.Class_of_Trade_Code__c : '';
        abuDate = Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss');
      
    }
    
}