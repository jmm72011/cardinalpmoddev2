/*************************************************************************************************************************************************************
@ Class:          ECMRestWebServiceCallout
@ Version:        1.0
@ Author:         Ranjan Samal
@ Purpose:        Rest Callout Class to make a POST Callout to test the ECM WS
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history: 08.10.2015 / Ranjan Samal / Created the class.
**************************************************************************************************************************************************************/

public class ECMRestWebServiceCallout {
    
    /*Constants*/
    private static final String CONTENT_TYPE            = 'Content-Type';
    private static final String COLON_SYMBOL            = ':';
    private static final String BEARER='Bearer ' ;//Added by Pavan for E7105
    private static final String AUTHORIZATION           = 'Authorization';
    private static final String POST                    = 'POST';
    private static final String WEBSERVICE_NAME         = 'ECMRestWebServiceCallout';
    private static final String ID                      = 'Id';
    private static final String ATTACHMENT_CONT_TYPE    = 'Attachment.ContentType';
    private static final String ONBOARDING_DEA_NUM      = 'Onboarding.DEA_Number__c';
    private static final String ONBOARDING_STAPH_NUM    = 'Onboarding.State_Pharmacy_License_Number__c';
    private static final String ONBOARDING_CDS_TYPE     = 'Onboarding.CDS_License_Number__c';
    private static final String ATTACHMENT_DESCRIP      = 'Attachment.Description';
    private static final String ONBOARDING_ACCT_NAME    = 'Onboarding.AccountName__c';
    private static final String ONBOARDING_SHPTO1_ADD   = 'Onboarding.Ship_To_Address_1__c';
    private static final String ONBOARDING_SHPTO2_ADD   = 'Onboarding.Ship_To_Address_2__c';
    private static final String ONBOARDING_SHPTO_CITY   = 'Onboarding.Ship_To_City__c';
    private static final String ONBOARDING_SHPTO_STATE  = 'Onboarding.Ship_To_State__c';
    private static final String ONBOARDING_SHPTO_ZIPCOD = 'Onboarding.Ship_To_Zip_Code__c';
    private static final String ONBOARDING_ID           = 'Onboarding.OnboardingID__c';
    private static final String ONBOARDING_BUSIN_UNIT   = 'Onboarding.Business_Unit__c';
    private static final String FILE_NAME               = 'File';
    private static final String DATA                    = 'data.txt';
    private static final String Document_Name           = 'DocumentName';
    private static final String CONTENT                 = 'content';
    private static final String CUSTOMER_ACCOUNT_NUMBER = 'Onboarding.Customer_Account_Number__c';
    
    private static final String CLASS_NAME           = 'ECMRestWebServiceCallout';
    private static final String METHOD_NAME          = 'sendNotification';
    private static final String LOG_TYPE             = 'ERROR';
    private static final String ALERT_TYPE           = 'APPLICATION_ALERTS';
    private final static String  ALERT_MESSAGE_EXE   = CLASS_NAME + '-Class: WebService Call Out Exception';  
    private final static String  ALERT_MESSAGE_DML   = CLASS_NAME + '-Class: DML Exception';  
    private static final String SEVERITY_CRITICAL    = 'CRITICAL';
    private static final String SEVERITY_HIGH        = 'HIGH';
    
    private static final String ECMCONNECTOR_ENDPOINT_URL = 'ECMConnector Endpoint URL';
    /*********************************************************************************************************************************************************
    @ Method:         sendNotification
    @ Version:        1.0
    @ Author:         RANJAN SAMAL(ranjan.samal@accenture.com)
    @ Purpose:        POST callout to submit the document to ECM
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:     attachContentType:    Attachment Content Type i.e the Type selected from UI
                      deaNumber:            DeaNumber of Onboarding record
                      stateLicenseNum:      State License Number of Onboarding record
                      attachDescr:          Description of the attachment uplaoded entered in the UI
                      accntName:            Account Name
                      address:              Address of the Onboarding record
                      city:                 City of the Onboarding record
                      state:                State of the Onboarding record
                      zipCode:              Zipcode of the Onboarding record
                      onBoardingId:         onBoardingId of the Onboarding record
                      businessUnit:         Business Unit of the Onboarding record
                      file:                 Base 64 encoded value of the attached file
                      fileName:             Name of the attached file
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        String -- ECM Link URL, where the document is uplaoded which is in turn displayed in the page on onboarding record
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Change history: 08.10.2015 / RANJAN SAMAL / Created the method.
    **********************************************************************************************************************************************************/
    public static String sendNotification(String attachContentType, String deaNumber, String stateLicenseNum, String cdsLicenseNum,
                                          String attachDescr, String accntName, String address, String city,
                                          String state, String zipCode, String onBoardingId, String businessUnit, String file, String fileName, String customerAccountNumber) {
                                              
        /*Fetching the request xml structure from the rest ebservice config object*/
        LIST<RestWSConfig__c> ecmReqs = [SELECT Request_Structure__c,
                                               WebServiceName__c 
                                          FROM RestWSConfig__c];
        String ecmLink;
        String reqString = ecmReqs[0].Request_Structure__c;
        HttpRequest req  = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http        = new Http();
        Blob headerValue;
        try {
        if(!system.test.isRunningTest()) {
            req.setEndpoint(Endpoints__c.getInstance(ECMCONNECTOR_ENDPOINT_URL).End_Point_URL__c);
        } else {
            req.setEndpoint('https://sg.ws.dev.cardinalhealth.com/Corp/Data/ECMContentSummaryService/V1_0/$x!CMConn/ContentFlat/$f!3548_A1001001A15I02B62248J75771v1');
        }
        //req.setEndpoint('https://sg.ws.dev.cardinalhealth.com/Corp/Data/ECMContentSummaryService/V1_0/$x!CMConn/ContentFlat/$f!3548_A1001001A15I02B62248J75771v1');
        /* TO DO --->>  Named Credentials to be implemented */
       string token;
       OAuthConnector x = new OAuthConnector();
       token= x.getoauthtoken();
        req.setMethod(POST);
       //req.setHeader(AUTHORIZATION, Endpoints__c.getInstance(ECMCONNECTOR_ENDPOINT_URL).Authorization_Header__c);
       req.setHeader( AUTHORIZATION, BEARER+ token ); 
       req.setHeader(CONTENT_TYPE,Endpoints__c.getInstance(ECMCONNECTOR_ENDPOINT_URL).Content_Type__c);
       req.setMethod(POST);
       headerValue = Blob.valueOf(Endpoints__c.getInstance(ECMCONNECTOR_ENDPOINT_URL).Username__c+ COLON_SYMBOL + Endpoints__c.getInstance(ECMCONNECTOR_ENDPOINT_URL).Password__c);
        
        /*Replacing the request structure with the actual values i.e. passed through the method from where its invoked*/
        reqString = reqString.replace(ATTACHMENT_CONT_TYPE,attachContentType);
        reqString = reqString.replace(ONBOARDING_DEA_NUM,deaNumber);
        reqString = reqString.replace(ONBOARDING_STAPH_NUM,stateLicenseNum);
        reqString = reqString.replace(ONBOARDING_CDS_TYPE,cdsLicenseNum);
        reqString = reqString.replace(ATTACHMENT_DESCRIP,attachDescr);
        reqString = reqString.replace(ONBOARDING_ACCT_NAME,accntName);
        reqString = reqString.replace(ONBOARDING_SHPTO1_ADD  + ONBOARDING_SHPTO2_ADD,address);
        reqString = reqString.replace(ONBOARDING_SHPTO_CITY,city);
        reqString = reqString.replace(ONBOARDING_SHPTO_STATE,state);
        reqString = reqString.replace(ONBOARDING_SHPTO_ZIPCOD,zipCode);
        reqString = reqString.replace(ONBOARDING_ID,onBoardingId);
        reqString = reqString.replace(ONBOARDING_BUSIN_UNIT,businessUnit);
        reqString = reqString.replace(FILE_NAME,file);
        reqString = reqString.replace(DATA,fileName);
        reqString = reqString.replace(Document_Name,fileName);
        reqString = reqString.replace(CUSTOMER_ACCOUNT_NUMBER,customerAccountNumber);
        
        
        system.debug('Request Data:' + reqString);
        
        req.SetBody(reqString);
        

        
                res = http.send(req);  
                //log the response
                  System.debug('received response : Status ' + res.getStatusCode());
                  System.debug('received response : Status ' + res.getStatus());
                  System.debug(res.getBody()); 

                Integer responseStatusCode = res.getStatusCode();                
                if(responseStatusCode < 300) {              
                    Dom.Document doc = res.getBodyDocument();
                    //Retrieve the root element for this document.
                    Dom.XMLNode node = doc.getRootElement();                        
                    // This prints out all the elements of the address
                    for(Dom.XMLNode child : node.getChildElements()) { 
                    system.debug('==============>>abhilash'+child.getName());                             
                        if(child.getName() == CONTENT){
                            if(child.getAttributeCount() > 0){
                                   for(integer i =1; i< child.getAttributeCount(); i++) {
                                       ecmLink = child.getAttributeValue(child.getAttributeKeyAt(i), child.getAttributeKeyNsAt(i));
                                       System.Debug('ECM Content URL received from ECM = ' + ecmLink);
                                      
                                       //switch ECM URL to single sign on URL -- Vasantha
                                       ecmlink = System.label.Redirect_ECM_to_SSO_URL + ecmlink.substring(ecmlink.indexOf(System.label.Replace_ECM_to_SSO_URL_Content, 0)-1); 
                                       System.Debug('ECM Content URL switched to Single Sign on URL = ' + ecmLink);
                                       ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Document Successfully uploaded on the ECM Server'));
                                    }   
                            }                          
                        }
                    }
                } else if (responseStatusCode  >= 300 && responseStatusCode  < 400){      
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Unable to upload document: ECM Server Network address problem')); 
                } else if (responseStatusCode  >= 400 && responseStatusCode  < 500){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Unable to upload document: Request Timeout or Invalid Data sent from Salesforce.com')); 
                } else if (responseStatusCode  >= 500 ){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Unable to upload document: ECM Server is not responding')); 
                } else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Unable to upload document: Unknown error')); 
                }
              /*Logging the Success Callout*/
                IntegrationUtility.logData(WEBSERVICE_NAME, System.Label.SAPMDG_Logtype, string.valueof(reqString),string.valueof(res), null);
                }
                catch (DmlException exp) {
                    /* -- Send alert to represented subscribers when callOut gets any DMLerror ---*/
                    ApexLogger.sendAlert(ALERT_MESSAGE_DML, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_HIGH );
                    exp.getMessage();
                }
                catch(System.CalloutException e) {
                
                    System.debug('Callout error: '+ e);
                    System.debug(res.toString());
                    
                    /*Logging the Failure Callout*/
                    IntegrationUtility.logData(WEBSERVICE_NAME, System.Label.SAPMDG_Failure_Logtype, 
                                                    string.valueof(reqString),string.valueof(res),e.getMessage());  
                                                    
                        System.Debug('Exception in CallSchedulerBatch : ' + e.getMessage());
                        System.Debug('Exception in CallSchedulerBatch : ' + e.getStackTraceString());                           
                    /* -- Send alert to represented subscribers when callOut gets any Exception ---*/
                    ApexLogger.sendAlert(ALERT_MESSAGE_EXE, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_CRITICAL );
            }
            return ecmLink;
        }
    }