/*************************************************************************************************************************************************************
@ Class:          COGSServiceREST
@ Version:        1.0
@ Author:         Bibekini Behera
@ Purpose:        REST Service class Implementation for COGSData Service
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/
public with sharing class COGSServiceREST extends RESTService
{
    final public static String SERVICENAME						='CogsService';
    final public static String SPLITDELIMITER					='-';
    final public static String DATEDELIMITER					='/';
    final public static String TIMEDELIMITER					='T';
    final public static String ERROR							='error'; 
    final public static String INNERERROR						='innererror'; 
	/*********************************************************************************************************************************************************
	@ Constructor:    COGSServiceREST
	@ Version:        1.0
	@ Author:         Bibekini Behera
	@ Purpose:        
	----------------------------------------------------------------------------------------------------------------------------------------------------------
	@ Change history:
	**********************************************************************************************************************************************************/
    public COGSServiceREST() { }

    /*********************************************************************************************************************************************************
    @ Method:         serviceCallout
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        Override Method - This is Required override method.  Implement this method
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        POST Continuation     - Returns Continuation
                      POST Non-Continuation - Returns HttpResponse
                      GET  Continuation     - Returns Continuation
                      GET  Non-Continuation - Returns HttpResponse
    **********************************************************************************************************************************************************/
    public static Object serviceCallout(Object appRequest, EndPoints__c serviceEndPoint, String serviceCallback) // String calloutMethod, String calloutType)
    {
        try{
        if (serviceEndPoint != null )
        {
            COGSServiceREST cogsService = new COGSServiceREST();
            String calloutMethod = serviceEndPoint.Method__c;
            /*if ( calloutMethod.equalsIgnoreCase(RESTService.POST))
            {
                //This service is not a POST Call.  No code required.
            }
            else */
            if ( calloutMethod.equalsIgnoreCase(RESTService.GET))
            {
                Object obj  = cogsService.createServiceRequestData((CustomerInfo) appRequest, calloutMethod);
                if(obj instanceof ServiceMessage){
                	return obj;
                }else{
                    System.Debug('serviceCallout: ---------------GET  CALLOUT----------------');
                    String getRequestQuery  = (String) obj;
                    System.Debug('getRequestQuery : ' + getRequestQuery);
                    return RestService.restServiceCallout(serviceEndPoint, serviceCallback, appRequest, getRequestQuery, cogsService);
                }
            }
        }
        return null;
       }catch(Exception e)
        {
            System.Debug('Exception: serviceCallout ' + e.getMessage() );
            System.Debug('Exception: serviceCallout ' + e.getStackTraceString() );
            ServiceMessage errorMessage = ServiceMessage.sendErrorMessage( 'Cogs serviceCallout Exception : ' + e.getMessage(), '', 'Retrieve Cogs' );
            return errorMessage;
           // return null;
        }
    }


    /*********************************************************************************************************************************************************
    @ Method:         createServiceRequest
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        Override Method - This is Required override method for both POST & GET Callouts.  Implement this method
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        POST Call - Return JSONGenerator Request Body
                      GET  Call - Return Get Query String
    **********************************************************************************************************************************************************/
    public virtual override Object createServiceRequestData(Object appRequest, String calloutMethod)
    {
        try{
            //INFO: In this Code block only create GET Query String
            CustomerInfo custInfoRequest = (CustomerInfo) appRequest;
            if ( !String.isEmpty(custInfoRequest.szSODCDV) )
            {
                custInfoRequest.getsodcdv(custInfoRequest.szSODCDV);
            }
            String enddate='';
            String startdate='';
            if(custInfoRequest.startdate!=null&& String.isNotBlank(custInfoRequest.startdate)){
                startdate=custInfoRequest.startdate.split(TIMEDELIMITER)[0].replace(SPLITDELIMITER,''); 
            }
            if(custInfoRequest.enddate!=null && String.isNotBlank(custInfoRequest.enddate)){
                enddate=custInfoRequest.enddate.split(TIMEDELIMITER)[0].replace(SPLITDELIMITER,''); 
            }

            String getQuery = 'Shipto+eq'+'\''+custInfoRequest.szCustomerNumber+'\''+'and+SalesOrg+eq'+'\''+custInfoRequest.SalesOrg+'\''+'and+DistrChannel+eq'+'\''+custInfoRequest.DistChannel+'\''+'and+Division+eq'+'\''+custInfoRequest.Division+'\''+'and+ToDate+ge'+'\''+enddate+'\''+'and+FromDate+le'+'\''+startdate+'\'';
        //String getQuery = 'Shipto+eq'+'\''+custInfoRequest.szCustomerNumber+'\''+'&+SalesOrg+eq'+'\''+custInfoRequest.SalesOrg+'\''+'&+DistrChannel+eq'+'\''+custInfoRequest.DistChannel+'\''+'&+Division+eq'+'\''+custInfoRequest.Division+'\''+'&+ToDate+ge'+'\''+custInfoRequest.enddate+'\''+'&+FromDate+le'+'\''+custInfoRequest.startdate+'\''; 
        // https://api.dev.cardinalhealth.com/pharma/sap/cogs/v1?$filter=Shipto eq '2050000114' and SalesOrg eq '2220' and DistrChannel eq '10' and Division eq '10' and ToDate ge '20191014' and FromDate le '20191014' and BuyingGroup eq '12121-AK'      
        //String endpoint=cogsEndpoint+'?$filter=Shipto+eq'+'\''+custInfo.szCustomerNumber+'\''+'and+Vkorg+eq'+'\''+custInfo.SalesOrg+'\''+'and+Vtweg+eq'+'\''+custInfo.DistChannel+'\''+'and+Spart+eq'+'\''+custInfo.Division+'\''+'and+Vtdat+le+datetime'+'\''+custInfo.startdate+'\''+'and+Vfdat+le+datetime'+'\''+custInfo.enddate+'\'';
            if(!String.IsBlank(custInfoRequest.classofTrade)){
                getQuery += 'and+BuyingGroup+eq'+'\''+custInfoRequest.classofTrade+'\'';
                //getQuery += '&+BuyingGroup+eq'+'\''+custInfoRequest.classofTrade+'\'';
                
            }
            System.Debug('createServiceRequestData : urlGETQuery : ' + getQuery );
            return getQuery;
            }
            catch (Exception e)
            {
                
                System.Debug('createServiceRequestData: Exception Occured: ' + e.getMessage());
                System.Debug('createServiceRequestData: Exception Occured: ' + e.getStackTraceString());
                ServiceMessage errorMessage = ServiceMessage.sendErrorMessage( 'Cogs createServiceRequestData Exception : ' + e.getMessage(), '', 'Retrieve Cogs' );
                return errorMessage;
            }
    }



   /*********************************************************************************************************************************************************
    @ Method:         retrieveResponseData
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        Override Method - This is Required override method.  Implement this method here
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        Response Data
    **********************************************************************************************************************************************************/
    public virtual override Object retrieveResponseData(HttpResponse response, Object appRequest)
    {
        try{
        System.Debug('TIMESTAMP:Entering COGSDataServiceREST : retrieveResponseData ' );
		CustomerInfo custInfo = (CustomerInfo) appRequest;
        //Error Handling Starts bb
        //changed as part of Error Handling 
        ServiceMessage errMsg;
        String restResponse=response.getBody();
        List<Integer> successCodes = new List<Integer>{200,201,202,204};

          //the status code is not 200 even if the error comes from sap not apigee,
          //APigee confirmed that they will copy sap resonse as it is including the status code and all  i.e why checking   
         //whether first key of response json is error before going to remap success response  
            if(!successCodes.contains(response.getStatusCode())){//if not success
       		Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(restResponse);
            Object objectNames =result.get(ERROR);
            if(objectNames!=null){
                System.debug('Error coming 1'+objectNames);
                String errorBody=String.valueOf(objectNames);
                if(errorBody.contains(INNERERROR)){
                   SAPErrorhandlingData sapError=(SAPErrorhandlingData) System.JSON.deserialize(restResponse, SAPErrorhandlingData.class);
                   SAPErrorhandlingData errData=new SAPErrorhandlingData();
                    errMsg=new ServiceMessage();
                   	errMsg= (ServiceMessage)errData.reMapSAPErrorResponse(sapError,response);
            	   system.debug('purchasehistory response'+sapError); 
                }else{
                    ApigeeErrorhandlingData apigeeerror=(ApigeeErrorhandlingData) System.JSON.deserialize(restResponse, ApigeeErrorhandlingData.class);
            	   	ApigeeErrorhandlingData errData=new ApigeeErrorhandlingData();
                    errMsg=new ServiceMessage();
                   	errMsg= (ServiceMessage)errData.reMapApigeeErrorResponse(apigeeerror,response);
                    system.debug('purchasehistory response'+apigeeerror);
                }
                
            }
            }
        if(errMsg!=null){
            errMsg.userMessage=SERVICENAME+' is failing due to some backend issue';
            if(errMsg.source!=null){
                errMsg.source.serviceName=SERVICENAME;
            } 
            custInfo.errors=new List<ServiceMessage>();
            custInfo.errors.add(errMsg);
            return custInfo;
        }
		//Error Handling ends bb	
		
        //Retreive this Service Response Data
        COGSServiceDATA serviceData = new COGSServiceDATA();
        //changed as part of Error Handling 
            Object obj =serviceData.retrieveJSONResponse(response);
        
            if(obj instanceof ServiceMessage){
                return obj;
            }else{    
        		COGSServiceDATA cogsResponse = (COGSServiceDATA) obj;
        		system.debug('COGSData response'+cogsResponse);
                //Remap the data and send it back to Caller
                if ( cogsResponse != null )
                {
                    return reMapResponse(cogsResponse, appRequest);
                }
                return custInfo;
            }
        }catch(Exception e){
           		System.Debug('retrieveResponseData: Exception Occured: ' + e.getMessage());
                System.Debug('retrieveResponseData: Exception Occured: ' + e.getStackTraceString());
                ServiceMessage errorMessage = ServiceMessage.sendErrorMessage( 'Cogs retrieveResponseData Exception : ' + e.getMessage(), '', 'Retrieve Cogs' );
                return errorMessage; 
        }
    }


/*********************************************************************************************************************************************************
    @ Method:         reMapResponse
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        Process Last Purchase History response
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:     Object of ProductCatalog, HttpResponse of COGSData
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        List of ProductItem of ProductCatalog class(List<ProductCatalog.ProductItem>)
    **********************************************************************************************************************************************************/
    public Object reMapResponse(Object response, Object appRequest)
    {
        try{
        if(response != null)
        {

            COGSServiceDATA cogsResponse= (COGSServiceDATA) response;
            system.debug('response deserialsed'+cogsResponse);
            if(cogsResponse!=null && cogsResponse.d!=null){
                List<COGSServiceDATA.COGS> cogsResponseList   = cogsResponse.d.results;
                System.debug('responseResult!!!!!!!!!'+cogsResponseList);
				CustomerInfo custInfo = (CustomerInfo) appRequest;
                custInfo.cogsList=new List<CustomerInfo.Cogs>();
            
            if(cogsResponseList != null && cogsResponseList.size()>0)
            {
                custInfo.addMessage('Success');//Success message
                CustomerInfo.Cogs customerResp;//changed to resolve P2 violation
                for(COGSServiceDATA.COGS cogresp : cogsResponseList){
                    customerResp  =new CustomerInfo.Cogs();
                    if(String.isNotBlank(cogresp.Soldto)) {
                        customerResp.soldTo             =cogresp.Soldto;
                    }
                    if(String.isNotBlank(cogresp.BuyingGroup)){
                        customerResp.clsofTrade =cogresp.BuyingGroup;
                    }
                    //String Vtdate                   =cogresp.Vtdat;
                    //String date1                    =Vtdate.substringAfter('(').substringBefore(')');
                    //string validtoDate              =string.valueOfGmt(datetime.newinstance(long.valueOf(date1)));

                    //System.debug('validtoDate@@@@'+validtoDate);
                   // customerResp.validTo            =cogresp.ToDate;
                    if(String.isNotBlank(cogresp.ToDate) && cogresp.ToDate.length()>=8){
                      customerResp.validTo=cogresp.ToDate.substring(6,8)+DATEDELIMITER+cogresp.ToDate.substring(4,6)+DATEDELIMITER+cogresp.ToDate.substring(0,4);
                    }
                    //String Vfdate                   =cogresp.Vfdat;
                    //String date2                    =Vfdate.substringAfter('(').substringBefore(')');
                    //string validfromDate            =string.valueOfGmt(datetime.newinstance(long.valueOf(date2)));

                    //System.debug('validfromDate@@@@'+validfromDate);
                   // customerResp.validFrom          =cogresp.FromDate;
                    if(String.isNotBlank(cogresp.FromDate) && cogresp.FromDate.length()>=8){
                       customerResp.validFrom=cogresp.FromDate.substring(6,8)+DATEDELIMITER+cogresp.FromDate.substring(4,6)+DATEDELIMITER+cogresp.FromDate.substring(0,4);
                    }
                    if(String.isNotBlank(cogresp.Hiercy)){
                        customerResp.priority           =cogresp.Hiercy;
                    }
                    if(String.isNotBlank(cogresp.Factr)){
                        customerResp.factor              =cogresp.Factr;
                    }
                    if(String.isNotBlank(cogresp.Mrkup)){
                        customerResp.additionalMarkup    =cogresp.Mrkup;
                    }
                    if(String.isNotBlank(cogresp.BuyingGroup)){
                        customerResp.clsofTrade    =cogresp.BuyingGroup;
                    }
                    if(String.isNotBlank(cogresp.Description)){
                        customerResp.description	  =cogresp.Description;
                    }
                    custInfo.cogsList.add(customerResp);
                    
                    /*Dummy Test Data for UI   customerResp.additionalMarkup   =85;       */            
                 }
            }
       // }
            System.debug('cogs response'+custInfo);
            return custInfo;
        }
        }
        return appRequest;
        }catch(Exception e) {
               	system.debug(e);
               	System.Debug('Exception in CogsService remap : ' + e.getMessage());
                System.Debug('Exception in CogsService remap: ' + e.getStackTraceString());
                ServiceMessage errorMessage = ServiceMessage.sendErrorMessage( ' CogsService remap Exception : ' + e.getMessage(), '', 'Retrieve Cogs Data' );
                return errorMessage; 
            }
}

}