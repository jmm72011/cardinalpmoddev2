/********************************************************************************************************************
@ Class:          OnboardingTriggerHelper 
@ Version:        1.0
@ Author:         Mohan Swarna(mohan.swarna@cardinalhealth.com)
@ Purpose:        Sending Onboarding Request to MDG after approved.
---------------------------------------------------------------------------------------------------------------------
@ Change history: 09.23.2015 / Mohan Swarna / Created the trigger.
@ Change history: 03.16.2020 / Accenture Jorge Mejia / added splitMaintenanceRequests method
*********************************************************************************************************************/
Public with sharing class OnboardingTriggerHelper{
    final static String AUTO_APPROVED_CM = '002';
    final static String APPROVAL_REQUIRED_CM = '001';
              final static string SUB_PENDING = 'Submit Pending';          //Added as part of Review Comments
              final static string CANCEL_SYSTEM = 'Cancelled by System';   //Added as part of Review Comments
              final static string CANCEL_USER = 'Cancelled by User';       //Added as part of Review Comments
              final static String CLASS_NAME           = 'OnbTriggerHelper';
              final static String METHOD_NAME          = 'callOnbdTrigger';
    final static String LOG_TYPE             = 'ERROR';
    final static String ALERT_TYPE           = 'APPLICATION_ALERTS';
              final static String SEVERITY_CRITICAL    = 'CRITICAL';
    
              
    public static Id maintnenanceRecTypeId = Schema.SobjectType.Onboarding__c.getRecordTypeInfosByDeveloperName().get('Maintenance').getRecordTypeId();
    public static Id shipToRecTypeId = Schema.SobjectType.Onboarding__c.getRecordTypeInfosByDeveloperName().get('ShipTo').getRecordTypeId();
    public static Id payerRecTypeId = Schema.SobjectType.Onboarding__c.getRecordTypeInfosByDeveloperName().get('Payer').getRecordTypeId();
    public static Id billToRecTypeId = Schema.SobjectType.Onboarding__c.getRecordTypeInfosByDeveloperName().get('BillTo').getRecordTypeId();

    
    @future(Callout = true) /* MDG Callout */
    /********************************************************************************************************************
    @ Method:         callOnbdTrigger
    @ Version:        1.0
    @ Author:         Mohan Swarna(mohan.swarna@cardinalhealth.com)
    @ Purpose:        Helper method to Call from trigger and calling SAPMDGConnector class.
    ---------------------------------------------------------------------------------------------------------------------
    @ Parameters :   onboardingIds     // Contains updated onboarding record ids 
    *********************************************************************************************************************/    
    Public static void callOnbdTrigger(Set<Id> onboardingIds){    
         
        /* Checking if List is not empty */ 
        if(!onboardingIds.isEmpty()){
             /* Iterating Approved Onboarding records  */
            for(string onbd : onboardingIds){               
                SAPMDGConnector.connectorMethod(onbd); /* calling SAPMDGConnector class for call out */
            }      
        }
                             
    } 
    @future(Callout = true)
    public static void submitCallout (list<id> argOnbdIds ) {
    system.debug('**: OnboardingTriggerHelper, passed ids: ' +  argOnbdIds );
        system.debug('**: OnboardingTriggerHelper, passed ids: ' +  argOnbdIds );
    // submit button callout to MDG (not Parmed)
        OnboardingMDGConnector onbdConnector = new OnboardingMDGConnector ();
            for (id onbrdId : argOnbdIds) {  // callout one soldto at a time (will recursive to children)
                onbdConnector .connectorMethod(onbrdId);
            }
    }
    
    /********************************************************************************************************************
    @ Method:         splitMaintenanceRequests
    @ CreatedDate:    3.16.2020
    @ Version:        1.0
    @ Author:         Accenture/Jorge Mejia(jorge.mejiav@cardinalhealth.com)
    @ Purpose:        Helper method split maintenance requests into multiple requests multiple request types
                      have been chosen by the user. Splitting is based on whether the request type can be sent
                      to MDG in a bundle or if it must be stand alone.
    ---------------------------------------------------------------------------------------------------------------------
    @ Parameters :   argMaintRecs     // Contains list of maintenance (onboarding object) records
    *********************************************************************************************************************/   
    public static void splitMaintenanceRequests(List<Onboarding__c> argMaintRecs){
                             
                             set <id> errIds = new set<id> ();
        List<Onboarding__c> multiTypeRequests = new List<Onboarding__c>();
        for(Onboarding__c onbd : argMaintRecs){          // In for loop null check not required though Review Comments given
                                 
            if(maintnenanceRecTypeId.equals(onbd.RecordTypeId)  && SUB_PENDING.equalsIgnoreCase(onbd.Onboarding_Status__c)){ //only run this when coming from submit button & dot equals implemented as part of Review Comments
                List<String> maintReqTypes = onbd.Account_Request_Type__c.split(';');
                if(maintReqTypes.size()>1) //checks if maintenance record has more than one request type selected
                    {multiTypeRequests.add(onbd);}
            errIds.add(onbd.id);
            }
        }
                             //Added Try Catch as part of Review Comments
        try{
        if(multiTypeRequests.size()>0)
            {handleSplitting(multiTypeRequests);}
		} catch (exception e) {
            system.debug('**splitMaintenanceRequests failed:' + e.getmessage() + errIds);
            list <exception>  liste = new list <Exception> ();
            liste.add(e);                   
            ApexLogger.sendAlert(liste, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_CRITICAL );                                
        }

    }

    /********************************************************************************************************************
    @ Method:         handleSplitting
    @ CreatedDate:    3.24.2020
    @ Version:        1.0
    @ Author:         Accenture/Jorge Mejia(jorge.mejiav@cardinalhealth.com)
    @ Purpose:        Helper method to split a maintenance record into multiple records to handle dependent request types
    ---------------------------------------------------------------------------------------------------------------------
    @ Parameters :   argMultiTypeRequests     // Contains list of maintenance (onboarding object) records with multple request types selected
    *********************************************************************************************************************/ 

    private static void handleSplitting(List<Onboarding__c> argMultiTypeRequests){

        //Try Catch Not Required as private method though Review Comments given
        List<Onboarding__c> childrenMaintToInsert = new List<Onboarding__c>();
        List<String> typesToRemove;
        List<Onboarding__c> childrenMaint;
        Boolean setParentReqType = false;
        

        //query metadata types and create a map of code to dependency value
        Maintenance_Request_Types_Reference__mdt[] typeReferences = [SELECT Backend_Code__c, Independent__c FROM Maintenance_Request_Types_Reference__mdt LIMIT 50000];
        Map<String, Boolean> typeDependencyMap = new Map<String, Boolean>();
        for(Maintenance_Request_Types_Reference__mdt typeRef : typeReferences)
            {typeDependencyMap.put(typeRef.Backend_Code__c, typeRef.Independent__c);}

            

        for(Onboarding__c maintRec : argMultiTypeRequests){   // For loop null check not required though Review Comments given
            List<String> maintReqTypes = maintRec.Account_Request_Type__c.split(';');
            typesToRemove = new List<String>();
            childrenMaint = new List<Onboarding__c>();
        
            for(String reqType : maintReqTypes){
                if(AUTO_APPROVED_CM.equalsIgnoreCase(reqType)){ //if Auto approved is selected, that is always assigned to parent && implemented dot equals as part of Review Comments
                    maintRec.Account_Request_Type__c = AUTO_APPROVED_CM;
                    typesToRemove.add(reqType);
                    setParentReqType = true;
        
                } else if (APPROVAL_REQUIRED_CM.equalsIgnoreCase(reqType) && !maintReqTypes.contains(AUTO_APPROVED_CM)){ // Approval required only assigned to parent if auto approved not selected & implemented .equals as part of Review Comments
                    maintRec.Account_Request_Type__c = APPROVAL_REQUIRED_CM;
                    typesToRemove.add(reqType);
                    setParentReqType = true;
        
                } else if (!typeDependencyMap.get(reqType) && childrenMaint.size() != maintReqTypes.size() - 1){ //Dependencies stand alone
                    Onboarding__c childDependentMaint = maintRec.clone(false, true, false, false);
                    childDependentMaint.Onboarding_Status__c = 'Draft';
                    childDependentMaint.OnboardingID__c = '';
                    childDependentMaint.Account_Request_Type__c = reqType;
                    childDependentMaint.Parent_Id__c = maintRec.Id;
                    typesToRemove.add(reqType);
                    childrenMaint.add(childDependentMaint);
                } else if (childrenMaint.size() == maintReqTypes.size()-1) { //handles situation when all dependent types chosen
                    maintRec.Account_Request_Type__c = reqType;
                    setParentReqType = true;
                    typesToRemove.add(reqType);
                }
            }

           //handles independent request types

            //remove values used in children from list of request types

            for(String reqType : typesToRemove)
                {maintReqTypes.remove(maintReqTypes.indexOf(reqType));}   
                
            //set request type for parent if not yet set
             if(!setParentReqType)
             {maintRec.Account_Request_Type__c = String.join(maintReqTypes, ';');}
        
            if(setParentReqType && maintReqTypes.size()>0){
                Onboarding__c childIndependentMaint = maintRec.clone(false, true, false, false);
                childIndependentMaint.Onboarding_Status__c = 'Draft';
                childIndependentMaint.OnboardingID__c = '';
                childIndependentMaint.Parent_Id__c = maintRec.Id;
                childIndependentMaint.Account_Request_Type__c = String.join(maintReqTypes, ';');
                childrenMaint.add(childIndependentMaint);
            }

            if(childrenMaint.size()>0)
                {childrenMaintToInsert.addAll(childrenMaint);}
        }
        
        //insert children maintenance records
        if(childrenMaintToInsert.size()>0)
            {insert childrenMaintToInsert;}
        
    }


    /********************************************************************************************************************
    @ Method:         cancelChildRecords
    @ CreatedDate:    3.26.2020
    @ Version:        1.0
    @ Author:         Accenture/Jorge Mejia(jorge.mejiav@cardinalhealth.com)
    @ Purpose:        Helper method to determine whether a maintenance record was cancelled, calls private method to handle
                      propagation of status to children records
    ---------------------------------------------------------------------------------------------------------------------
    @ Parameters :   argMaintRecs     // Contains list of maintenance (onboarding object) records
    *********************************************************************************************************************/   
    public static void cancelChildRecords(List<Onboarding__c> argMaintRecs){
                             set <id> errIds = new set<id> ();
        Map<Id, String> cancelledReqsToStatus = new Map<Id, String>();
        for(Onboarding__c maint : argMaintRecs){
            if(maintnenanceRecTypeId.equals(maint.RecordTypeId) && (CANCEL_SYSTEM.equalsIgnoreCase(maint.Onboarding_Status__c) || CANCEL_USER.equalsIgnoreCase(maint.Onboarding_Status__c))) //Implemented dot equals as part of Review Comments
                {cancelledReqsToStatus.put(maint.Id, maint.Onboarding_Status__c);}
                                                          errIds.add(maint.id);
        }
                             // Added Try Catch as part of Review Comments
        try{
        if(cancelledReqsToStatus.size()>0)
            {handleCancel(cancelledReqsToStatus);}
        }catch (exception e) {
            system.debug('**cancelChildRecords failed:' + e.getmessage() + errIds);
            list <exception>  liste = new list <Exception> ();/*  */
            liste.add(e);                   
            ApexLogger.sendAlert(liste, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_CRITICAL );                  
        }
    }


    /********************************************************************************************************************
    @ Method:         handleCancel
    @ CreatedDate:    3.26.2020
    @ Version:        1.0
    @ Author:         Accenture/Jorge Mejia(jorge.mejiav@cardinalhealth.com)
    @ Purpose:        Helper method to change the status of children maintenance records to cancelled when a parent 
                      maintennace record's status is changed to cancelled.
    ---------------------------------------------------------------------------------------------------------------------
    @ Parameters :   argCancelledReqsToStatus     // Contains map of parent maintenance (onboarding object) record ids with their cancelled status
    *********************************************************************************************************************/  

    private static void handleCancel(Map<Id, String> argCancelledReqsToStatus){
              //Try Catch Not Required as private method though Review Comments given
        List<Onboarding__c> childrenToUpdate = new List<Onboarding__c>();
        for(Onboarding__c childMaint : [SELECT Id, Onboarding_Status__c, Parent_Id__c FROM Onboarding__c WHERE Parent_Id__c IN :argCancelledReqsToStatus.keySet() LIMIT 50000]){
            childMaint.Onboarding_Status__c = argCancelledReqsToStatus.get(childMaint.Parent_Id__c);
            childrenToUpdate.add(childMaint);
        }
            
        if(childrenToUpdate.size()>0)
            {update childrenToUpdate;}

    }

    /********************************************************************************************************************
    @ Method:         updateActAsChkBoxes
    @ CreatedDate:    3.26.2020
    @ Version:        1.0
    @ Author:         Accenture/Jorge Mejia(jorge.mejiav@cardinalhealth.com)
    @ Purpose:        
    ---------------------------------------------------------------------------------------------------------------------
    @ Parameters :   argOnbRecs     // Contains list of new onboarding records
    *********************************************************************************************************************/  

    public static void updtActAsChkBoxes(List<Onboarding__c> argOnbRecs){
        set <id> errIds = new set<id> ();
        List<Onboarding__c> recsWithParent = new List<Onboarding__c>();
        Set<Id> parentIds = new Set<Id>();
        for(Onboarding__c onb : argOnbRecs){
            if(onb.Parent_Id__c != null && (String.valueOf(shipToRecTypeId).equalsIgnoreCase(String.valueOf(onb.RecordTypeId)) || String.valueOf(billToRecTypeId).equalsIgnoreCase(String.valueOf(onb.RecordTypeId)) || String.valueOf(payerRecTypeId).equalsIgnoreCase(String.valueOf(onb.RecordTypeId))) ){
                recsWithParent.add(onb);
                parentIds.add(onb.Parent_Id__c);
            }
        }

        try {
            if(recsWithParent.size()>0){
                handleUpdtChkBoxes(recsWithParent, parentIds);
            }
        } catch (Exception e) {
            system.debug('**updtActAsChkBoxes failed:' + e.getmessage() + errIds);
            list <exception>  liste = new list <Exception> ();/*  */
            liste.add(e);                   
            ApexLogger.sendAlert(liste, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_CRITICAL ); 
            
        }
        
       
        
    }


    private static void handleUpdtChkBoxes(List<Onboarding__c> argOnbRecs, Set<Id> argParentIds){

        Map<Id, Onboarding__c> parentOnbRecs = new Map<Id, Onboarding__c>([SELECT Id, Act_as_Bill_to__c, Act_as_Payer__c, Act_as_Ship_to__c FROM Onboarding__c WHERE Id IN : argParentIds LIMIT 50000 FOR UPDATE]);

        String recTypeId;
        String strShipToRecTypeId = String.valueOf(shipToRecTypeId);
        String strBillToRecTypeId = String.valueOf(billToRecTypeId);

        for(Onboarding__c onb : argOnbRecs){
            recTypeId = String.valueOf(onb.RecordTypeId);

            if(recTypeId.equalsIgnoreCase(strShipToRecTypeId)){
                parentOnbRecs.get(onb.Parent_Id__c).Act_as_Ship_to__c = false;
                continue;

            }

            if(recTypeId.equalsIgnoreCase(strBillToRecTypeId)){
                parentOnbRecs.get(onb.Parent_Id__c).Act_as_Bill_to__c = false;
                continue;
                
            }

            parentOnbRecs.get(onb.Parent_Id__c).Act_as_Payer__c = false;
            
        }

        update parentOnbRecs.values();

    }

}