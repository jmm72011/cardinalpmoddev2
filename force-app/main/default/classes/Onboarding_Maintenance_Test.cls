/*************************************************************************************************************************************************************
@ Class:          Onboarding_Maintenance_Test
@ Version:        1.0
@ Author:         Abhirup Banik
@ Purpose:        Test Class for Onboarding button on Prospect Account and Maintenance button
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/
@isTest
global class Onboarding_Maintenance_Test {    
    
    private static final String ONBOARDING_RECTYPE_SOLDTO  = 'SoldTo';  
    private static final String ONBOARDING_RECTYPE_SHIPTO  = 'ShipTo';
    private static final String ONBOARDING_RECTYPE_BILLTO  = 'BillTo';
    private static final String ONBOARDING_RECTYPE_PAYER = 'Payer';
    
    private static final String ONBOARDING_BUTTON_SOLDTO  = 'SoldTo';  
    private static final String ONBOARDING_BUTTON_SHIPTO  = 'ShipTo';
    private static final String ONBOARDING_BUTTON_BILLTO  = 'BillTo';
    private static final String ONBOARDING_BUTTON_PAYER = 'Payer';

    @TestSetup
    static void makeData(){

        insert new ActiveTriggers__c( name = 'P-MOD TRIGGER CONTROL', AccountTrigger__c = true);

        List<Account> accounts = TestDataFactory.createAccounts(1, 'CAH Customer');
        accounts[0].Time_Zone__c = 'EST';
		accounts[0].FaxExt__c = '1234';        
        update accounts;

        list<Cardinal_Business_Unit__c> cbulist = TestDataFactory.CardinalBUdataInsert(1);
        Id cbuId = cbulist[0].Id;
        cbulist[0].Name = 'PD';
        cbulist[0].Active__c = 'Yes';
        update cbulist;
        
        list<SAP_Sales_Area__c> acBUlist = TestDataFactory.SAPSalesAreadataInsert(accounts,cbulist,1);
        acBUlist[0].Business_Unit__c = cbuId;
        acBUlist[0].Commit_Time__c = '00:00:00';
        update acBUlist;
        
        User u = TestDataFactory.createUser('System Administrator');
        insert u;
        u.Business_Unit__c = 'PD';
        update u;
    }
    
    static testmethod void testMain(){
        
        Account acct = [SELECT Id, FaxExt__c FROM Account WHERE Name = 'Test Account 0' AND RecordType.DeveloperName = 'CAH_Customer' LIMIT 1];   
        SAP_Sales_Area__c abu = [SELECT Id FROM SAP_Sales_Area__c WHERE Account__c = :acct.Id];     
        Cardinal_Business_Unit__c cbu = [SELECT Id FROM Cardinal_Business_Unit__c WHERE Name = 'PD' LIMIT 1];
        User u = [Select id FROM User where alias = 'usr' LIMIT 1];
        system.debug('account Id: ' + acct.Id);
        system.debug('onb button sold to: ' + ONBOARDING_BUTTON_SOLDTO);
        
        Onboarding__c TestOnboard1 = new Onboarding__c(Name = 'Test Onbaord1',
             AccountName__c = acct.Id,
             RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId(),
             Servicing_Group__c = 'NATIONAL ACCOUNT',
             Cardinal_Business_Unit__c = cbu.Id,                                         
             Account_Request_Type__c ='001; 006',
             Customer_Account_Group__c = 'ZPST',
             Onboarding_Status__c ='Draft',
             Payment_Type__c = 'ACH',
             Payment_Terms__c = 'Weekly Credit Card',
             Bill_To_Country__c = 'USA',
             Ship_To_Country__c = 'USA',
             Bill_To_Invoice_Method__c = 'Invoice - Print',
             Business_Unit__c ='PD',
             //Cardinal_business_unit__c = cardinalBURecs[0].id,//Added By Shreyashi
             EDI_Setup__c = true,
             Sticker_Perference_del__c='Price Sticker with Pricing',
             KYC__c='No',
             Has_Completed_the_Credit_Application__c= true,
             Initially_Purchase_Controlled_Substance__c= true,
             Rebate_Preferences__c='Credit',
             Lot_Expiration_Preferences__c='30',
             CSOS__c= true,
             Approved__c = false,
             Priority__c='Normal',
             Bill_To_License_Name__c='tesrty',
             Bill_To_Telephone__c='123654789',
             Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
             Ship_To_Email_Address__c='test@tset.com',
             Ship_To_Fax_Number__c='515225-3170',
             Ship_To_Phone_Number__c='515225-3170',
             Bill_To_Zip_Code__c='55440',
             Bill_To_City__c='MINNEAPOLIS',
             Bill_To_State__c='MN',
             Bill_To_Address1__c='PO BOX 9493',
             Ship_To_Zip_Code__c='45678',
             Ship_To_City__c='city',
             Ship_To_State__c='IA',
             Request_Type__c='Full Setup',
             Sync_Fields__c = false,
             Credit_Limit__c=234.00,
             Customer_Classification__c='sss',
             Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
             //Estimated_First_Purchase_Date__c = system.today(),
             Is_this_new_business_to_Cardinal_Health__c ='Yes,',
             //Class_of_Trade_Level_1__c = '01 - HOSPITAL;',
             Time_Zone__c = 'EST',
             Fax_Ext__c = 1234,
             Requested_Credit_Limit__c =123.00);
        
        Test.startTest();
            //Allow_Duplicate_Purchase_Orders__c='Yes');
            insert TestOnboard1;
            System.runas(u)
            {
                String val1 = Onboarding_Maintenance.OnboardingMaint(acct.Id, abu.Id);
                try{//Exception handling 
                Onboarding_Maintenance.OnboardingMaint(null, null);
                }catch(Exception e){
                system.assertEquals(e.getMessage(),'ERROR : Account not found');
                }
                Onboarding_Maintenance.OnboardingfromProspect(acct.Id,ONBOARDING_RECTYPE_SOLDTO);
                Onboarding_Maintenance.OnboardingfromProspect(acct.Id,ONBOARDING_RECTYPE_SHIPTO);
                try{//Exception handling 
                Onboarding_Maintenance.OnboardingfromProspect(acct.Id,ONBOARDING_BUTTON_BILLTO);
                }catch(Exception e){
                system.assertEquals(e.getMessage(), 'Record type parameter must be SoldTo or ShipTo');
                }                
                Onboarding_Maintenance.OnboardingfromAccount(acct.Id,ONBOARDING_BUTTON_SOLDTO);
                Onboarding_Maintenance.OnboardingfromAccount(acct.Id,ONBOARDING_BUTTON_SHIPTO);
                Onboarding_Maintenance.OnboardingfromAccount(acct.Id,ONBOARDING_BUTTON_BILLTO);
                Onboarding_Maintenance.OnboardingfromAccount(acct.Id,ONBOARDING_BUTTON_PAYER);
                try{//Exception handling
                Onboarding_Maintenance.OnboardingfromAccount(acct.Id,'OTHERS');
                }catch(Exception e){
                system.assertEquals(e.getMessage(), 'Record type parameter must be SoldTo or ShipTo');
                } 
                Onboarding_Maintenance.AccountSync(TestOnboard1.id,acct.Id);
                Onboarding_Maintenance.syncOnboardingFields_Parmed(TestOnboard1.id);
                Onboarding__c onb = [SELECT Id, Fax_Ext__c FROM Onboarding__c WHERE Id = :val1];
                System.debug('Queried Onb+++'+onb);
            }
        Test.stopTest();
    }
    
}