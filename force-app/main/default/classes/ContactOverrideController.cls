/*******************************************************************************************************************************************************************************
    @ Class:          ContactOverrideController 
    @ Version:        1.0
    @ Author:         SEEMESH PATEL(seemesh.patel@accenture.com)
    @ Purpose:        Case custom Controller to customize a standerd page 
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Change history: 10.10.2016 / SEEMESH PATEL/ Created the class.
    ********************************************************************************************************************************************************************************/
    public with sharing class ContactOverrideController {
            /*Constants */
            final static String     RETRNURL        = 'retURL';
            final static String     RECORDTYPE      = 'RecordType';
            final static String     PMOD            = 'PMOD';
            final static String     MED             = 'MED';
            final static String     APEXCONTACTID   = 'apex/EnhancedSmartContactSearch?id=';
            final static String     APEXPMODCONTACT = '/apex/EnhancedSmartContactSearch';
            final static String     SLASH           = '/';
            final static String     SLASHE          = '/e';
            final static String     SPACE           = ' ';
            final static String     ERROR_MESSAGE   = 'needs to be added to custom setting';
            final static String     APEXMEDCONTACT  = '/apex/SmartContactSearch';
			
            final static String     NOOVERRIDE      = 'nooverride';
            final static String     ONE             = '1';
        
        private ApexPages.StandardController controller;
        public  String                       retURL        { get; set; }
        public  String                       rType         { get; set; }
        public  Contact                         con         { get; set; }       /*Blank Case Field Representation*/
        
        /***************************************************************************************************************************************************************************
        @ Constructor:    ContactOverrideController(ApexPages.StandardSetController controller)
        @ Version:        1.0
        @ Author:         SEEMESH PATEL (seemesh.patel@accenture.com)
        @ Purpose:        set property for Adding New Custom Button in List View
        ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        @ Change history: 10.10.2016 / SEEMESH PATEL / Created the Constructor. 
        ****************************************************************************************************************************************************************************/
        public ContactOverrideController (ApexPages.StandardController controller) {
            con = new Contact();
            this.con  = (Contact)controller.getRecord();
            retURL = ApexPages.currentPage().getParameters().get(RETRNURL);
            rType = ApexPages.currentPage().getParameters().get(RECORDTYPE);
            
        }
        
        /***************************************************************************************************************************************************************************
        @ Method:         redirectPage
        @ Version:        1.0
        @ Author:         SEEMESH PATEL (seemesh.patel@accenture.com)
        @ Purpose:        redirectPage to Pharma: Custom Case Page OR Med : Standard Page
        ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        @ Change history: 10.10.2016 / SEEMESH PATEL / Created the method. 
        *****************************************************************************/
        public PageReference redirectPage() {
    
            PageReference returnURL;
    
            // Redirect if Record Type corresponds to custom VisualForce page
            Cardinal_Health_Profile__c parmedProfiles = Cardinal_Health_Profile__c.getValues(userinfo.getProfileId());
           
            if (parmedProfiles.App_Type__c == PMOD) {
            
                returnURL = con.Id != Null ? (new PageReference(APEXCONTACTID+con.Id)) : (new PageReference(APEXPMODCONTACT));
    
            } else  if (parmedProfiles.App_Type__c == MED){
                returnURL = con.Id != Null ? (new PageReference(SLASH+con.Id+SLASHE)) : (new PageReference(APEXMEDCONTACT));
            } else {
               
               id id1 = userinfo.getProfileId();
                profile pname = [select Name from profile where id = :id1];
               string profileName = pname.Name;
               
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,profileName + SPACE + ERROR_MESSAGE));
                return  Null;
            }
        
    
            returnURL.getParameters().put(RETRNURL, retURL);
            returnURL.getParameters().put(RECORDTYPE, rType);
            returnURL.getParameters().put(NOOVERRIDE, ONE);
            returnURL.setRedirect(true);
            return returnURL;
        }
    }