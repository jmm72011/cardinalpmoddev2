/*************************************************************************************************************************************************************
@ Class:          RESTService
@ Version:        1.0
@ Author:         Vasantha Nelahonne
@ Purpose:        Abstract Parent Class for all REST Services.   Class also Extends the REST Service functionalities to support OAuth and Continuation.
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history: 05-16-2019 Created
**************************************************************************************************************************************************************/


public virtual class RESTService extends ContinuationService {

    private final static String AUTHORIZATION = 'Authorization';
    private final static String XAPIKEY = 'x-api-key';
    private final static String APIAPP='x-cah-api-app';
    private final static String APIGUID='x-cah-api-guid';
    private final static String APIPROVIDER='x-cah-api-provider';
    private final static String ACCEPT='Accept';
    private final static String XREQ='X-Requested-With';
    private static final String ENCODING_TYPE ='UTF-8';
    private static final String CONTENT = 'Content';
    private static final String CONNECTION = 'Connection';
    private static final String KEEP_ALIVE = 'keep-alive';
    private static final String ACCEPT_ENC = 'Accept-Encoding';
    private static final String ENCODING_VAL = 'gzip,deflate';
    private static final String USER_AGENT = 'User-Agent';
    private static final String MSG_TIMESTAMP = 'x-cah-msg-timestamp';    //This is a dynamic time to represent callout initiated time.


    public final static String POST = 'POST';
    public final static String GET = 'GET';
    public final static String CONTINUATION_REST_CALL = 'CONTINUATION_REST_CALL';
    public final static String SYNCHRONOUS_REST_CALL = 'SYNCHRONOUS_REST_CALL';
    public final static Integer TIMEOUT = 120000;

    private final static String CONTENT_TYPE = 'Content-Type';
    private static final String CLASS_NAME                          = 'RESTService';
    private static final String LOG_TYPE                            = 'ERROR';
    private static final String ALERT_TYPE                          = 'APPLICATION_ALERTS';
    private final static String ALERT_MESSAGE_EXE                   = 'WebService Exception';
    private final static String ALERT_MESSAGE_DML                   = 'DML Exception';
    private static final String SEVERITY_CRITICAL                   = 'CRITICAL';
    private static final String SEVERITY_HIGH                       = 'HIGH';
    private static final String FORMAT_FILTER                       = '$format=json&$filter=';

    //ECM Document Interface Specific Attributes
    private static final String  ECM_USER_AGENT_VAL = 'Apache-HttpClient/4.1.1 (java 1.5)';
    private static final String  ECM_DOCUMENT_DOWNLOAD_ENDPOINT = 'PH_IBM_ECM_DocumentDownloadService';



    //OAUTH CALLOUTS
    //Note from Vasantha: As a temporary solution we will make separate callout for each Service request to get a new Token
    //This is VERY INEFFECIENT SOLUTION because of 2 reasons.  It is a synchronous callout.  Each Service requires a refreshed token for every call
    //WE NEED to Find alternative solution to refresh the token Per User session or Application Session.
    //Better solution is to implement Salesforce OAuth with JWT.
    //ONCE BETTER SOLUTION IS IMPLEMENTED, PLEASE TURN OFF THIS ADD_OAUTH_CALLOUT FLAG
    private static final boolean ADD_OAUTH_CALLOUT  = TRUE;
   //ADDED BY KIRANMAI
    public static boolean BASIC_OUTH_FLOW  = FALSE;

    /*Constants - end*/

    public static Endpoints__c getEndPoint(String endPoint)
    {
        try {
                System.Debug('EndPoint : ' + endPoint);
                Endpoints__c  serviceEndPoint = Endpoints__c.getInstance(endPoint);
                if ( serviceEndPoint == null)
                {
                    System.Debug('getEndPoint Error : EndPoint not Found ' + endPoint);
                }
            return serviceEndPoint;
        }
        catch(Exception e)
        {
            System.Debug('getEndPoint Exception : ' +  e.getMessage());
            System.Debug('getEndPoint Exception : ' +  e.getStackTraceString());
        }
        return null;
    }

    public HTTPRequest serviceRequestHEADER(HTTPRequest request, EndPoints__c serviceEndPoint)
    {
        System.Debug('EndPoint serviceEndPoint : ' + serviceEndPoint);
        String method = serviceEndPoint.Method__c;
        if ( serviceEndPoint != null )
        {
           //Check if ServiceEndPoint is Vendavo added by bibekini
            if(serviceEndPoint.name.equalsIgnoreCase('PH_VENDAVO_Pricing')){
                request= OAuthConnectorVendavo.addOAuthTokenFromVendavo(request);
                //request.setClientCertificateName('cardinal_gateway_vendavo');//cardinal_gateway_vendavo
                system.debug('OAuthConnectorVendavoRequest' +request);
            }
            //BASIC AUTHENTICATION ADDED BY KIRANMAI
            else if ( BASIC_OUTH_FLOW )
            {
            request.setEndpoint(serviceEndPoint.End_Point_URL__c);    
                
                String username = serviceEndPoint.Username__c; 
                String password =serviceEndPoint.passWord__c; 
                  
                Blob headerValue =  Blob.valueOf(username + ':' + password);
                String authorizationHeader = 'Basic ' +EncodingUtil.base64Encode(headerValue);
                request.setHeader('Authorization',authorizationHeader);
            }
            else if ( ADD_OAUTH_CALLOUT )
            {
                request = OAuthConnector.addOAuthToken(request);
            }
            
            

            //Add Start Time for all callouts for tracking
             if(!serviceEndPoint.name.equalsIgnoreCase('PH_VENDAVO_Pricing')){
                 request.setHeader(MSG_TIMESTAMP, String.valueof(DateUtil.todayDateTimeMS_UTC()));
             }
            //req.setHeader(AUTHORIZATION, serviceEndPoint.Authorization_Header__c);
            if (String.isNotBlank(serviceEndPoint.X_API_KEY__c))
            {
                request.setHeader(XAPIKEY, serviceEndPoint.X_API_KEY__c);
            }

            //Set Content Type
            if (String.isNotBlank(serviceEndPoint.Content_Type__c))
            {
                request.setHeader(CONTENT_TYPE, serviceEndPoint.Content_Type__c);

            }
            request.setMethod(method);

            if (method.equalsIgnoreCase(POST))
            {
                request.setEndpoint(serviceEndPoint.End_Point_URL__c);
            }
            //SET Api App
            if(String.isNotBlank(serviceEndPoint.x_cah_api_app__c))
            {
                request.setHeader(APIAPP, serviceEndPoint.x_cah_api_app__c);
            }

            //Set guid to identify each unique service transaction id for tracking
           if(String.isNotBlank(serviceEndPoint.x_cah_api_guid__c))
            {
                request.setHeader(APIGUID, serviceEndPoint.x_cah_api_guid__c + '_' + DateUtil.transactionGUID());
            }
            else if(!serviceEndPoint.name.equalsIgnoreCase('PH_VENDAVO_Pricing'))//changed for vendavo
            {
                request.setHeader(APIGUID, String.valueOf(DateUtil.transactionGUID()));
            }

         

            //set api provider
            if(String.isNotBlank(serviceEndPoint.x_cah_api_provider__c)){
                request.setHeader(APIPROVIDER, serviceEndPoint.x_cah_api_provider__c);
            }

            if(String.isNotBlank(serviceEndPoint.Accept__c)){
                request.setHeader(ACCEPT, serviceEndPoint.Accept__c);
            }

            if(String.isNotBlank(serviceEndPoint.X_Requested_With__c))
            {
                request.setHeader(XREQ,serviceEndPoint.X_Requested_With__c);
            }

            //Timeout
            if(serviceEndPoint.Time_Out__c != null)
            {
                request.setTimeout(serviceEndPoint.Time_Out__c.intValue());
                System.debug('TimeOut in CustomSettings' + serviceEndPoint.Time_Out__c.intValue());
            }
            else
            {
                //default
                request.setTimeout(TIMEOUT);
                System.debug('Constant TimeOut:' + TIMEOUT);
            }

            //ECM Document Download specific Header Settings
            //if(serviceEndPoint.name.equalsIgnoreCase(ECM_DOCUMENT_DOWNLOAD_ENDPOINT))
             if(serviceEndPoint.name.equalsIgnoreCase('Invoice') || serviceEndPoint.name.equalsIgnoreCase('Pro-Forma Invoice') ||
                serviceEndPoint.name.equalsIgnoreCase('Credit Memo') || serviceEndPoint.name.equalsIgnoreCase('MRA') || serviceEndPoint.name.equalsIgnoreCase( 'PH_IBM_ECM_DocumentFileDownloadService'))
           {
                System.Debug('REQUEST HEADERS: Adding Headers for ECM ');
                request.setHeader(ACCEPT_ENC,ENCODING_VAL);
                request.setHeader(CONNECTION,KEEP_ALIVE);
                request.setHeader(USER_AGENT,ECM_USER_AGENT_VAL);
            }
            
              if(serviceEndPoint.name.equalsIgnoreCase('PH_VENDAVO_Pricing')){
              request.setClientCertificateName('cardinal_gateway_vendavo');//cardinal_gateway_vendavo
                system.debug('OAuthConnectorVendavoRequest' +request);
            }

        }
        System.Debug('REQUEST HEADERS:' + request);
        logRESTServiceRequest(request);
        return request;
    }

    //returns Continuation
    //public static Object restServiceCallout(String serviceEndPoint, String serviceCallback, Object appRequest, String calloutMethod, String calloutType, Object calloutRequest, Object restServiceInstance )
    public static Object restServiceCallout(EndPoints__c serviceEndPoint, String serviceCallback, Object appRequest, Object calloutRequest, Object restServiceInstance)
    {

        String calloutType = serviceEndPoint.Call_Type__c;
        String calloutMethod = serviceEndPoint.Method__c;
        if(calloutType.equalsIgnoreCase(CONTINUATION_REST_CALL ))
        {

            if (calloutMethod.equalsIgnoreCase(POST))
            {
                return serviceContinuationCalloutPOST(serviceEndPoint, serviceCallback, appRequest, calloutRequest);
            }
            else if (calloutMethod.equalsIgnoreCase(GET))
            {
                return serviceContinuationCalloutGET(serviceEndPoint, serviceCallback, appRequest, calloutRequest);

            }
        }
        else if (calloutType.equalsIgnoreCase(SYNCHRONOUS_REST_CALL))
        {
            if (calloutMethod.equalsIgnoreCase(POST))
            {
                return serviceSynchronousCalloutPOST(serviceEndPoint, appRequest, calloutRequest, restServiceInstance);
            }
            else if (calloutMethod.equalsIgnoreCase(GET))
            {
                return serviceSynchronousCalloutGET(serviceEndPoint, appRequest, calloutRequest, restServiceInstance);
            }
        }
        return null;
    }

    //returns Continuation
    public  static Continuation serviceContinuationCalloutPOST(EndPoints__c serviceEndPoint, String serviceCallback, Object appRequest, Object calloutRequest)
    {

            System.Debug('Entering: RESTService: serviceContinuationCalloutPOST : ');
            //DateTime strtTime = dateTime.now();
            RESTService rest = new RESTService();
            //System.Debug('Exiting: RESTService: cart.ServiceEndTime'+cart.ServiceEndTime);
            JSONGenerator jsonRequest = (JSONGenerator) calloutRequest;
            HTTPRequest restRequest = rest.prepareHttpRequestPOST(serviceEndPoint, jsonRequest);  //inherited RESTService
            logRESTServiceRequest(restRequest);
            System.Debug('Exiting: RESTService: serviceContinuationCalloutPOST');
            return continuationCallout(restRequest, serviceCallback, appRequest);

    }

    //returns Continuation
    public  static Continuation serviceContinuationCalloutGET(EndPoints__c serviceEndPoint, String serviceCallback, Object appRequest, Object calloutRequest)
    {

            System.Debug('Entering: RESTService: serviceContinuationCalloutGET  : ');
            RESTService rest = new RESTService();
            String getQuery = (String) calloutRequest;
            HTTPRequest restRequest = rest.prepareHttpRequestGET(serviceEndPoint, getQuery);  //inherited RESTService
            logRESTServiceRequest(restRequest);
            System.Debug('Exiting: RESTService: serviceContinuationCalloutGET');
            return continuationCallout(restRequest, serviceCallback, appRequest);

    }

    //returns processed Response Data
    public static Object serviceSynchronousCalloutPOST(EndPoints__c serviceEndPoint, Object appRequest, Object calloutRequest,  Object restServiceInstance )
    {

            RESTService restInstance = (RestService) restServiceInstance;
            JSONGenerator jsonRequest;
            HTTPRequest request;
            //changes for i3 by Bibekini added if else 
            if(calloutRequest instanceOf JSONGenerator){
                jsonRequest = (JSONGenerator) calloutRequest;
                request = restInstance.prepareHttpRequestPOST(serviceEndPoint, jsonRequest);  //inherited RESTService
            }else{
                request = restInstance.prepareHttpRequestPOSTXML(serviceEndPoint, calloutRequest); 
            }
            
            //Callout and get a Response
            HTTPResponse response = restInstance.sendRESTCallout(request);

            //call the override method to process response
            return restInstance.retrieveResponseData(response, appRequest);

    }

    //returns processed Response Data
    public static Object serviceSynchronousCalloutGET(EndPoints__c serviceEndPoint, Object appRequest, Object calloutRequest,  Object restServiceInstance )
    {

            RESTService restInstance = (RestService) restServiceInstance;
            String getQuery = (String) calloutRequest;
            HTTPRequest request = restInstance.prepareHttpRequestGET(serviceEndPoint, getQuery);  //inherited RESTService

            //Callout and get a Response
            HTTPResponse response = restInstance.sendRESTCallout(request);

            //call the override method to process response
            return restInstance.retrieveResponseData(response, appRequest);
    }


    public HttpRequest prepareHttpRequestPOST(EndPoints__c serviceEndPoint, JSONGenerator jgen)
    {
        system.debug('json string'+jgen);
        HttpRequest request = new HttpRequest();
        request = serviceRequestHEADER(request, serviceEndPoint);
        System.Debug('request:  ' + request);
        if( jgen != null)
           request.setBody(jgen.getAsString());

        return request;
    }
    //changes for i3 by Bibekini added below method
    public HttpRequest prepareHttpRequestPOSTXML(EndPoints__c serviceEndPoint, Object xmlString)
    {
        system.debug('json string'+xmlString);
        HttpRequest request = new HttpRequest();
        request = serviceRequestHEADER(request, serviceEndPoint);
        System.Debug('request:  ' + request);
        if( xmlString != null)
           request.setBody((String)xmlString);

        return request;
    }

    public HttpRequest prepareHttpRequestGET(EndPoints__c serviceEndPoint, String getQuery)
    {
        HttpRequest request = new HttpRequest();
        request = serviceRequestHEADER(request, serviceEndPoint);

        String requestURL = serviceEndPoint.END_Point_URL__c;
        System.Debug('Service EndPoint Name : ' + serviceEndPoint.name);
        if(String.isNotBlank(getQuery))
        {

//            if(serviceEndPoint.name.equalsIgnoreCase('Invoice') || serviceEndPoint.name.equalsIgnoreCase('Credit Memo') || serviceEndPoint.name.equalsIgnoreCase('MRA'))
             if(serviceEndPoint.name.equalsIgnoreCase('Invoice') || serviceEndPoint.name.equalsIgnoreCase('Pro-Forma Invoice') ||
                serviceEndPoint.name.equalsIgnoreCase('Credit Memo') || serviceEndPoint.name.equalsIgnoreCase('MRA') || serviceEndPoint.name.equalsIgnoreCase( 'PH_IBM_ECM_DocumentFileDownloadService'))
            {
                requestURL = requestURL + '?' + getQuery;

            }
            else
            {
                requestURL = requestURL + '?' + FORMAT_FILTER + getQuery;
            }
        }
        System.Debug('requestURL : ' + requestURL);
        System.Debug('request:  ' + request);
        request.setEndpoint(requestURL);

        return request;
    }

    //for Parallel calls
    public  static Continuation parallelRestServiceCallout(List<EndPoints__c> serviceEndPoints, String serviceCallback, Object appRequest, List<Object> calloutRequests)
    {

            System.Debug('Entering: RESTService: serviceContinuationCalloutGET  : ' + serviceEndPoints);
            List<HTTPRequest> restRequestList = new List<HTTPRequest>();

            if ( serviceEndPoints != null && serviceEndPoints.size() > 0)
            {

                restRequestList = parallelPrepareHttpRequestList(serviceEndPoints, calloutRequests);  //inherited RESTService
            }
            List<String> serviceNames = new List<String>();
            for(Integer i=0; i < serviceEndPoints.size() ; i++)
            {
                serviceNames.add(serviceEndPoints[i].name);
            }

            return parallelContinuationCallout(restRequestList, serviceNames, serviceCallback, appRequest);

    }


    //for Parallel Calls
    public  static List<HTTPRequest> parallelPrepareHttpRequestList(List<EndPoints__c> serviceEndPoints,  List<Object> calloutRequests)
    {

            System.Debug('Entering: RESTService: serviceContinuationCalloutGET  : ');
            List<HTTPRequest> requestList = new List<HTTPRequest>();
            RESTService rest = new RESTService();

            for ( Integer i = 0; i < serviceEndPoints.size(); i++)
            {
                HTTPRequest restRequest = new HTTPRequest();
                if(serviceEndpoints[i].method__c.equalsIgnoreCase(GET))
                {
                    restRequest = rest.prepareHttpRequestGET(serviceEndPoints[i], (String) calloutRequests[i]);
                }
                if(serviceEndpoints[i].method__c.equalsIgnoreCase(POST))
                {
                    restRequest = rest.prepareHttpRequestPOST(serviceEndPoints[i], (JSONGenerator) calloutRequests[i]);
                }
                logRESTServiceRequest(restRequest);
                if(restRequest != null)
                {
                    requestList.add(restRequest);
                }
            }

            return requestList;

    }


    public HttpResponse sendRESTCallout(HTTPRequest request)
    {
        
        try
        {
            
            logRESTServiceRequest(request);
            datetime strtTime = system.now();
            Http objHttp = new Http();
        
           //id i=ContinuationService.InsertIntegrationLog(null);
            HTTPResponse response = objHttp.send(request);
           // ContinuationService.InsertIntegrationLog(i);
          
          
            system.debug('HTTP Response Status code:'+ response.getStatusCode());
            System.debug('HTTP Response Body:'+ response.getBody());
            logRESTServiceResponse(response);
            return response;
        }
        catch(Exception e)
        {
            System.Debug('RESTCallout : Exception occured : ' + e.getMessage());
            System.Debug('RESTCallout : Exception occured : ' + e.getStackTraceString());
            return null;
        }
    }

    /*********************************************************************************************************************************************************
    @ Method:         createServiceRequestData
    @ Version:        1.0
    @ Author:         Vasantha Nelahonne
    @ Purpose:        NOTE: DO NOT CHANGE THIS METHOD.  This is required for for both POST & GET.
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        null -- for error
    **********************************************************************************************************************************************************/
    public virtual Object createServiceRequestData(Object request, String calloutMethod)
    {
        System.Debug('TIMESTAMP:Entering createServiceRequest ' );
        System.Debug('ContinuationService : createServiceRequest: Error: Vasantha Info: this is override method in super class.  This method should never be Invoked.');
        return null;
    }

    /*********************************************************************************************************************************************************
    @ Method:         retrieveResponseData
    @ Version:        1.0
    @ Author:         Vasantha Nelahonne
    @ Purpose:        NOTE: DO NOT CHANGE THIS METHOD.  Implement call back in extended implementation of this Class
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        null -- for error
    **********************************************************************************************************************************************************/
    //public virtual Object retrieveResponseData(ContinuationService cs)
    public virtual Object retrieveResponseData(HttpResponse response, Object appRequest)
    {
        System.Debug('TIMESTAMP:Entering retrieveResponseData ' );
        System.Debug('ContinuationService : retrieveResponseData: Error: Vasantha Info: this method should never be Invoked.');
        return null;
    }

    /*********************************************************************************************************************************************************
    @ Method:         serviceResponse
    @ Version:        1.0
    @ Author:         Vasantha Nelahonne
    @ Purpose:        Returns the ContinuationService which contains extracted data
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        ContinuationService
    **********************************************************************************************************************************************************/
    public Object serviceResponse(Object state)
    {
        System.Debug('TIMESTAMP:Entering RESTService : serviceResponse ' );
        System.Debug('RESTService : serviceResponse: state : ' + state);

        //Retreive the response  back
        //ContinuationService.ContinuationState calloutState = (ContinuationService.ContinuationState) continuationCallback(state);
        ContinuationService cs = (ContinuationService) continuationServiceResponse(state);
        System.Debug('continuationCallback : ' + cs);
        System.Debug('continuationCallback :  cs.serviceResponse : ' + cs.serviceResponse);
        logRESTServiceResponse(cs.serviceResponse);
/************
//added by Kiran 

IntegrationLog__c logObj = new IntegrationLog__c();
            logObj.ServiceStart__c = system.now();
            logObj.ServiceEnd__c = system.now();
            
            insert logObj;
************************************************************/            
        //return this.retrieveResponseData(cs);
        return this.retrieveResponseData(cs.serviceResponse, cs.calloutState.requestData);
    }


    public List<ContinuationService> parallelServiceResponses(Object state)
    {
        System.Debug('TIMESTAMP:Entering RESTService : serviceResponse ' );
        System.Debug('RESTService : serviceResponse: state : ' + state);

        //Retreive the response  back
        return (List<ContinuationService>) continuationServiceResponseList(state);

    }

   //Vasantha : Generic Method to print Request Headers
   public static void logRESTServiceRequest(HttpRequest request)
   {
       if ( request != null )
       {
               //Log Service Headers
                System.Debug('Request Header: URL : ' + request.getEndpoint());
                String [] keys = new String[]{ 'Content-Type', 'Authorization', 'x-api-key', 'Accept', 'X-Requested-With','x-cah-api-app','x-cah-api-guid','x-cah-api-provider', 'Accept-Encoding', 'Content', 'Connection','User-Agent', 'x-cah-msg-timestamp'};

                for ( integer i = 0; i < keys.size(); i++ )
                {
                    if ( request.getHeader(keys[i]) != null )
                    {
                        System.Debug( 'Request Header: ' + keys[i] + ' : ' + request.getHeader(keys[i]) );
                    }

                }
                //Log Body
                System.Debug( 'Request Body: ' + request.getBody() );
                String body = 'Request Body: ' + request.getBody();
                logStringUntruncated(body);
       }
   }

   public static void logRESTServiceResponse(HttpResponse response)
   {
       if ( response != null )
       {
                //log Headers
                for(string keys : response.getHeaderKeys())
                {
                   system.debug('Response Header: ' + keys + ' : ' + response.getHeader(keys));
                }

                //Log Body
                System.Debug( 'Response Body: ' + response.getBody() );
                String body = 'Response Body: ' + response.getBody();
                logStringUntruncated(body);
       }
   }


    /*********************************************************************************************************************************************************
    @ Method:         logStringUntruncated
    @ Version:        1.0
    @ Author:         Vasantha Nelahonne
    @ Purpose:        Salesforce debug log truncates strings very long.  Use this method to log full string data without any truncation
                      This method breaks the string into multiple lines log.  Use this method especially when logging Service Response from SAP
    **********************************************************************************************************************************************************/
    public static void logStringUntruncated(String str) {
        for (Integer i = 0; i < str.length(); i=i+255) {
            Integer endIndex = (i+255 > (str.length()-1) ? str.length()-1 : i+255);
            System.debug(str.substring(i,endIndex));
        }
    }
}