/*************************************************************************************************************************************************************
@ Class    :        AccountTriggerHelper
@ Version  :        1.0
@ Author   :        Mohan Swarna (Mohan.swarna@accenture.com)
@ Purpose  :        Helper Class for AccountTrigger. 
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history: 14.12.2015 / Mohan Swarna / Created the Class.
**************************************************************************************************************************************************************/

Public with sharing class AccountTriggerHelper {

    private static final String CLASS_NAME  = 'AccountTriggerHelper';   /*Constant class name for APEX LOG info*/
    private static final String METHOD_NAME = 'deleteAssignments';     /*Constant Method name for APEX LOG info*/    
    /*********************************************************************************************************************************************************
    @ Method:         deleteAssignments
    @ Version:        1.0
    @ Author:         Mohan Swarna (Mohan.swarna@accenture.com)
    @ Purpose:        To delete account assignment records when Restricted_Prospect__c flag is checked
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameter:      accountIds:  Contains new accountIds to delete related assignment records
    @ Change history: 14.12.2015 / Mohan Swarna / Created the Method.
    *********************************************************************************************************************************************************/
    public static void deleteAssignments(Set<Id> accountIds){
        
        /* Retrieving Account Assignment records*/
        List<Account_Assignment__c> accntAssigns = [SELECT Id,
                                                           Account__c,
                                                           Account__r.Restricted_Prospect__c
                                                      FROM Account_Assignment__c
                                                     WHERE Account__c IN: accountIds 
                                                       AND Account__r.Restricted_Prospect__c = TRUE LIMIT 50000];
        /* Checking if list is not empty and deleting account assignment records */              
        if(!accntAssigns.isEmpty()) {
            try {                
                Database.delete(accntAssigns, True);
                if(Test.isRunningTest()) {
                    integer intTest =1/0;
                }
            } catch (Exception ex) {
                ApexLogger.logError(new List<Exception> { ex }, CLASS_NAME, METHOD_NAME); 
            }
        }
    }
    /*********************************************************************************************************************************************************
    @ Method:         deleteAssignmentsForInactiveAccounts
    @ Version:        1.0
    @ Author:         Sravya Penumaka (sravya.penumaka@accenture.com)
    @ Purpose:        To delete account assignment records when the status of the Account is Inactive
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameter:      accountIds:  Contains new accountIds to delete related assignment records
    @ Change history: 14.12.2015 / Mohan Swarna / Created the Method.
    *********************************************************************************************************************************************************/
    public static void deleteAssignmentsForInactiveAccounts(Set<Id> accountIds){
        
        /* Retrieving Account Assignment records*/
        List<Account_Assignment__c> accntAssignRecs = [SELECT Id,
                                                           Account__c,
                                                           Account__r.account_Status__c
                                                      FROM Account_Assignment__c
                                                     WHERE Account__c IN: accountIds 
                                                       AND Account__r.account_Status__c = 'Inactive' LIMIT 50000];
        system.debug('[Sravya]Account Assignment records'+accntAssignRecs);                                           
        /* Checking if list is not empty and deleting account assignment records */             
       if(!accntAssignRecs.isEmpty()) {
            try {                
                Database.delete(accntAssignRecs, True);
            } catch (Exception ex) {
                ApexLogger.logError(new List<Exception> { ex }, CLASS_NAME, METHOD_NAME); 
            }
        }
    }
    
    /*********************************************************************************************************************************************************
    @ Method:         legacyAccountRestriction
    @ Version:        1.0
    @ Author:         Mohan Swarna (Mohan.swarna@accenture.com)
    @ Purpose:        To update Legacy Account Sales Area Restricted checkbox
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameter:      accountIds:  Contains new accountIds to delete related assignment records
    @ Change history: 14.12.2015 / Mohan Swarna / Created the Method.
    *********************************************************************************************************************************************************/
    public static void legacyAccountRestriction(List<Account> newAccounts){
        /* Retrieving legacy Record TypeId from Account  */
       try{
        RecordType legacyRecordID = [SELECT Id
                                       FROM RecordType
                                      WHERE SobjectType = 'Account' 
                                        AND Name = 'CAH Customer - Pharma' LIMIT 50000];
        /* Retrieving Account Sales Area records which is related to legacy account */
        List<SAP_Sales_Area__c> ssa = [ SELECT Id, 
                                               Name, 
                                               Restricted_Accounts__c, 
                                               Account__c, 
                                               Account__r.Name,
                                               Account__r.RecordTypeId,
                                               Account__r.Restricted_Prospect__c
                                          FROM SAP_Sales_Area__c
                                          WHERE Account__c IN :newAccounts 
                                          AND Account__r.RecordTypeId = :legacyRecordID.Id LIMIT 50000];                                
        /* Checking if account sales area list is not empty*/
        
        if(!ssa.isEmpty()){
            
            List<Sap_Sales_Area__c> updateSalesAreaList = new List<Sap_Sales_Area__c>();
            for ( SAP_Sales_Area__c salesArea : ssa ){
            
                //If the Legacy Account is  modified 
                if(salesArea.Account__r.RecordTypeId == legacyRecordID.Id){
                
                    //If and only if Legacy Restricted Prospect is modified, update the sales area Restricted Account checkbox
                    if ( salesArea.Restricted_Accounts__c != salesArea.Account__r.Restricted_Prospect__c){
                    
                        salesArea.Restricted_Accounts__c = salesArea.Account__r.Restricted_Prospect__c;
                        updateSalesAreaList.add(salesArea);
                    }
                }
            }

            //Update the Sales Area Restricted Account field 
            if(!updateSalesAreaList.isEmpty()){
                Database.Update (updateSalesAreaList,True);
            }
        }
        }catch(Exception e){
        System.debug('Exception has occurred: ' + e.getMessage());
        
        }
    }
    /*********************************************************************************************************************************************************
    @ Method:         emailNotificationToSalesRep
    @ Version:        1.0
    @ Author:         Mohan Swarna (Mohan.swarna@accenture.com)
    @ Purpose:        To Sending an email alert to Sales Reps when customer account is created in salesforce.
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameter:      updatedAccs :  Contains new Customer account records
    @ Parameter:      oldAccounts :  Contains Old Prospect account records
    @ Change history: 14.12.2015 / Mohan Swarna / Created the Method.
    *********************************************************************************************************************************************************/
    public static void emailNotificationToSalesRep(List<Account> updatedAccs, Map<Id,Account> oldAccounts){
        try{
        Set<Id> onboardingIds = new Set<Id>();
        RecordType CustomerAccountRecordID = [SELECT Id
                                                FROM RecordType 
                                                WHERE SobjectType = 'Account' AND Name = 'CAH Customer'LIMIT 50000];
                                                       
        List<Account_Assignment__c> updatedAssignments = new  List<Account_Assignment__c>();
        for(Account accnt : updatedAccs){
            if(accnt.RecordTypeId != oldAccounts.get(accnt.Id).RecordTypeId  && accnt.RecordTypeId == CustomerAccountRecordID.Id && oldAccounts.get(accnt.Id).Onboarding_ID__c == accnt.Onboarding_ID__c){
                onboardingIds.add(accnt.Onboarding_ID__c);
            }
        }
       
       /* List<Account_Assignment__c> accountAssignments = [SELECT Id,
                                                                 Send_Email_Notification__c,
                                                                 Account__r.Onboarding_ID__c
                                                            FROM Account_Assignment__c WHERE Account__r.Onboarding_ID__c IN: onboardingIds LIMIT 50000];
       */
       
       /*Medical Accounts CPU Error issue - The above query is returns Pharma accounts when Medical Accounts are updated.
         For Medical Accounts Following Query should not return any records */
         
        List<Account_Assignment__c> accountAssignments = [SELECT Id,
                                                                 Send_Email_Notification__c,
                                                                 Account__r.Onboarding_ID__c
                                                            FROM Account_Assignment__c 
                                                            WHERE Account__r.Onboarding_ID__c IN: onboardingIds
                                                            AND   Account__c IN: updatedAccs LIMIT 50000];
       
        System.Debug('Total Account Assignments Found : ' + accountAssignments.size());
        
        for(Account accnt : updatedAccs){
            if(accnt.RecordTypeId != oldAccounts.get(accnt.Id).RecordTypeId  && accnt.RecordTypeId == CustomerAccountRecordID.Id && oldAccounts.get(accnt.Id).Onboarding_ID__c == accnt.Onboarding_ID__c){
                for(Account_Assignment__c accAssign : accountAssignments){
                    if(accAssign.Account__c == accnt.Id){
                        accAssign.Send_Email_Notification__c = true;
                        updatedAssignments.add(accAssign);
                    }
                }
            }
        }
        if(!updatedAssignments.isEmpty()){
            Database.update(updatedAssignments, True);
        }
      }catch(Exception e){
        System.debug('Exception has occurred: ' + e.getMessage());
        }
      }
    /*********************************************************************************************************************************************************
    @ Method:         updateOnboardingAddress
    @ Version:        1.0
    @ Author:         Priyanka Dwivedi (p.dwivedi@accenture.com)
    @ Purpose:        To Update the Order Ready Onboarding Addresses if the Customer Account address is changed while conversion
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameter:      convertedAccounts :  Contains new Customer account records
    @ Parameter:      oldAccountsRecords :  Contains Old Prospect account records
    @ Change history: 29.03.2016 / Priyanka Dwivedi / Created the Method.
    *********************************************************************************************************************************************************
    public static void updateOnboardingAddress(List<Account> convertedAccounts, Map<Id,Account> oldAccountsRecords){
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accountParentIds = new Set<Id>();
        // Retrieving Customer Account Record Type
        RecordType CustomerAccountRecordID = [SELECT Id
                                                FROM RecordType 
                                                WHERE SobjectType = 'Account' AND Name = 'CAH Customer' LIMIT 50000];
        RecordType ProspectAccountRecordID = [SELECT Id
                                                FROM RecordType 
                                                WHERE SobjectType = 'Account' AND Name = 'CAH Prospect'LIMIT 50000];                                        
        for(Account accnt : convertedAccounts){
            if(accnt.RecordTypeId == CustomerAccountRecordID.Id ||
               accnt.RecordTypeId == ProspectAccountRecordID.Id ){
                accountIds.add(accnt.Id);
                // fecthing Parent Id's
                accountParentIds.add(accnt.ParentId);
             }
        }
        //fetching all the Onboardings for the Converted Customer Accounts
        List<Onboarding__c>  updatedOnboardings = new List<Onboarding__c>();
        List<Onboarding__c>  oldOnboardings = [SELECT    Id,
                                                         AccountName__c,
                                                         Bill_To_Address1__c,
                                                         Request_Type__c,
                                                         Bill_To_City__c,
                                                         Bill_To_Country__c,
                                                         Bill_To_Zip_Code__c,
                                                         Ship_To_Address_1__c,
                                                         Ship_To_City__c,
                                                         Ship_To_Country__c,
                                                         Servicing_Group__c,
                                                         Ship_To_Zip_Code__c,
                                                         Onboarding_Status__c ,
                                                         Store_Hours__c,
                                                         Secondary_Supplier__c,
                                                         Customer_Group__c,
                                                         Customer_Group_1__c,
                                                         Credit_Limit__c,
                                                         Date_KYC_Completed__c,
                                                         Ship_To_Phone_Number__c,
                                                         Ship_To_Phone_Number_Ext__c,
                                                         Ship_To_Fax_Number__c,
                                                         Ship_To_Fax_Number_Ext__c,
                                                         Ship_To_Email_Address__c,
                                                         Parent_Account_Number__c,
                                                         Buying_Group__c,
                                                         Secondary_Buying_Group__c,
                                                         Primary_Wholesaler__c,
                                                         Parent_Account__c,
                                                         Ship_To_DBA_Name__c,
                                                         Ship_To_License_Name__c,
                                                         Bill_To_License_Name__c,
                                                         Has_Completed_the_Credit_Application__c,
                                                         Existing_Cardinal_Account_Number__c
                                                   FROM  Onboarding__c
                                                   WHERE AccountName__c IN: accountIds Limit 50000];
        List<Account>       accounts    =  [SELECT       Id,
                                                         Name
                                                   FROM Account
                                                   WHERE Id IN:accountParentIds Limit 50000];
        // Creating Map to store the ParentId and associated Name                                          
        Map<Id,String> accountParentMap = new Map<Id,String>();
        for(Account acc : accounts){
        accountParentMap.put(acc.id,acc.Name);       
        }                                       
        for(Account accnt : convertedAccounts){  
            for(Onboarding__c onboard : oldOnboardings){
                if(onboard.AccountName__c == accnt.id && onboard.Onboarding_Status__c != 'Completed'){
                    // Checking if the Customer Accounts fields are Modfied 
                    if( accnt.ShippingStreet              != oldAccountsRecords.get(accnt.Id).ShippingStreet        ||
                        accnt.ShippingState               != oldAccountsRecords.get(accnt.Id).ShippingState         ||
                        accnt.ShippingCountry             != oldAccountsRecords.get(accnt.Id).ShippingCountry       ||
                        accnt.ShippingCity                != oldAccountsRecords.get(accnt.Id).ShippingCity          ||        
                        accnt.ShippingPostalCode          != oldAccountsRecords.get(accnt.Id).ShippingPostalCode    || 
                        accnt.BillingStreet               != oldAccountsRecords.get(accnt.Id).BillingStreet         ||
                        accnt.BillingState                != oldAccountsRecords.get(accnt.Id).BillingState          ||
                        accnt.BillingCountry              != oldAccountsRecords.get(accnt.Id).BillingCountry        ||
                        accnt.BillingCity                 != oldAccountsRecords.get(accnt.Id).BillingCity           ||   
                        accnt.Store_Hours__c              != oldAccountsRecords.get(accnt.Id).Store_Hours__c        ||
                        accnt.Secondary_Supplier__c       != oldAccountsRecords.get(accnt.Id).Secondary_Supplier__c ||
                        accnt.Servicing_Group__c          != oldAccountsRecords.get(accnt.Id).Servicing_Group__c    ||
                        accnt.Customer_Group__c           != oldAccountsRecords.get(accnt.Id).Customer_Group__c  ||
                        accnt.Customer_Group_1__c         != oldAccountsRecords.get(accnt.Id).Customer_Group_1__c ||
                        accnt.Credit_Limit_Parmed__c      != oldAccountsRecords.get(accnt.Id).Credit_Limit_Parmed__c ||
                        accnt.Date_KYC_Completed__c       != oldAccountsRecords.get(accnt.Id).Date_KYC_Completed__c ||    
                        accnt.Phone                       != oldAccountsRecords.get(accnt.Id).Phone ||                    
                        accnt.Phone_Ext__c                != oldAccountsRecords.get(accnt.Id).Phone_Ext__c ||
                        accnt.Fax                         != oldAccountsRecords.get(accnt.Id).Fax ||
                        accnt.FaxExt__c                   != oldAccountsRecords.get(accnt.Id).FaxExt__c ||
                        accnt.Email_Address__c            != oldAccountsRecords.get(accnt.Id).Email_Address__c ||
                        accnt.ParentId                    != oldAccountsRecords.get(accnt.Id).ParentId ||
                        accnt.Parent_Account_Number_Sold_To__c != oldAccountsRecords.get(accnt.Id).Parent_Account_Number_Sold_To__c ||
                        accnt.Buying_Group__c             != oldAccountsRecords.get(accnt.Id).Buying_Group__c ||
                        accnt.Secondary_Buying_Group__c   != oldAccountsRecords.get(accnt.Id).Secondary_Buying_Group__c ||
                        accnt.Primary_Wholesaler__c       != oldAccountsRecords.get(accnt.Id).Primary_Wholesaler__c ||
                        accnt.Customer_Number_Ship_To__c  != oldAccountsRecords.get(accnt.Id).Customer_Number_Ship_To__c ||
                        accnt.DBA_Account_Name__c!= oldAccountsRecords.get(accnt.Id).DBA_Account_Name__c||
                        accnt.Has_Completed_Credit_App__c != oldAccountsRecords.get(accnt.Id).Has_Completed_Credit_App__c||
                        accnt.BillingPostalCode           != oldAccountsRecords.get(accnt.Id).BillingPostalCode){
                        // Updating all the Onboarding Address fields
                        if(accnt.ShippingStreet != Null){ 
                            onboard.Ship_To_Address_1__c      = accnt.ShippingStreet;
                        }
                        if(accnt.ShippingCity != Null){ 
                            onboard.Ship_To_City__c           = accnt.ShippingCity;
                        }
                        if(accnt.ShippingCountry != Null){                    
                            onboard.Ship_To_Country__c        = accnt.ShippingCountry ;
                        }
                        if(accnt.ShippingState != Null){ 
                            onboard.Ship_To_State__c          = accnt.ShippingState ;
                        }
                        if(accnt.ShippingPostalCode != Null){ 
                            onboard.Ship_To_Zip_Code__c       = accnt.ShippingPostalCode;
                        }
                        if(accnt.BillingStreet !=Null){
                            onboard.Bill_To_Address1__c       = accnt.BillingStreet;
                        }
                        if(accnt.BillingCity != Null){
                            onboard.Bill_To_City__c           = accnt.BillingCity;
                        }
                        if(accnt.BillingCountry != Null){
                            onboard.Bill_To_Country__c        = accnt.BillingCountry ;
                        }
                        if(accnt.BillingState != Null){ 
                            onboard.Bill_To_State__c          = accnt.BillingState ;
                        }
                        if(accnt.BillingPostalCode != Null){ 
                            onboard.Bill_To_Zip_Code__c       = accnt.BillingPostalCode;   
                        } 
                        if(accnt.Servicing_Group__c != Null){
                        onboard.Servicing_Group__c        = accnt.Servicing_Group__c;
                        }
                        onboard.Store_Hours__c            = accnt.Store_Hours__c ;  
                        onboard.Secondary_Supplier__c     = accnt.Secondary_Supplier__c;  
                        onboard.Customer_Group__c         = accnt.Customer_Group__c;  
                        onboard.Customer_Group_1__c       = accnt.Customer_Group_1__c; 
                        if(accnt.Credit_Limit_Parmed__c != Null){  
                            onboard.Credit_Limit__c           = accnt.Credit_Limit_Parmed__c;
                        }  
                        onboard.Date_KYC_Completed__c     = accnt.Date_KYC_Completed__c; 
                        if(accnt.Phone != Null){ 
                            onboard.Ship_To_Phone_Number__c   = accnt.Phone;  
                        }
                        onboard.Ship_To_Phone_Number_Ext__c = accnt.Phone_Ext__c;  
                        onboard.Ship_To_Fax_Number__c     = accnt.Fax;  
                        onboard.Ship_To_Fax_Number_Ext__c = accnt.FaxExt__c;  
                        onboard.Ship_To_Email_Address__c  = accnt.Email_Address__c; 
                        // Checking if the Parent Id is present in the Map created earlier
                        if(accountParentMap.containskey(accnt.ParentId)){ 
                        onboard.Parent_Account__c         = accountParentMap.get(accnt.ParentId);
                        }
                        onboard.Parent_Account_Number__c  = accnt.Parent_Account_Number_Sold_To__c;  
                        onboard.Parent_Account_Number__c  = accnt.Parent_Account_Number_Sold_To__c;  
                        onboard.Buying_Group__c           = accnt.Buying_Group__c;  
                        onboard.Secondary_Buying_Group__c = accnt.Secondary_Buying_Group__c;  
                        onboard.Primary_Wholesaler__c     = accnt.Primary_Wholesaler__c; 
                        onboard.Existing_Cardinal_Account_Number__c = accnt.Customer_Number_Ship_To__c;                         
                        onboard.Ship_To_DBA_Name__c       = accnt.DBA_Account_Name__c;  
                        onboard.Has_Completed_the_Credit_Application__c = accnt.Has_Completed_Credit_App__c;  
                        onboard.Bill_To_License_Name__c   = accnt.Name;
                        if(accnt.Name != Null){ 
                        onboard.Ship_To_License_Name__c   = accnt.Name;
                        }
                        
                         
                        //Switch the request type to support validation rule
                        System.debug('AccountTriggerHelper:updateOnboardingAddress: Incoming Account Record Type ID = ' + accnt.RecordTypeId);
                         System.debug('AccountTriggerHelper:updateOnboardingAddress: Prospect Record Type ID = ' + ProspectAccountRecordID.Id);
                        System.debug('AccountTriggerHelper:updateOnboardingAddress: Customer Record Type ID = ' + CustomerAccountRecordID.Id);
                      System.debug('AccountTriggerHelper:updateOnboardingAddress: Record Type = ' + accnt.RecordType.Name);
                        System.debug('AccountTriggerHelper:updateOnboardingAddress: Onboarding Request Type = ' + onboard.Request_Type__c);
                        if (accnt.RecordTypeId == ProspectAccountRecordID.Id )
                        {
                            if (onboard.Request_Type__c == 'Initial Setup' )    //if Full Setup, don't change it
                             {
                                 onboard.Request_Type__c   = 'Initial to Full Setup';        
                             }
                             else 
                             {
                                 onboard.Request_Type__c   = 'Full Setup';   
                             }
                          System.debug('AccountTriggerHelper:updateOnboardingAddress: Onboard Request type changed = ' + onboard.Request_Type__c);
                        }
                        else if ( accnt.RecordTypeId == CustomerAccountRecordID.Id )
                        {
                             if (onboard.Request_Type__c != 'Full Setup' )    //if Full Setup, don't change it
                             {
                                 onboard.Request_Type__c   = 'Initial to Full Setup';        
                             }
                             else
                             {
                                 onboard.Request_Type__c   = 'Full Setup';        
                           
                             }
                           System.debug('AccountTriggerHelper:updateOnboardingAddress: Onboard Request type changed = ' + onboard.Request_Type__c);
                       }
                       else
                       {
                            onboard.Request_Type__c   = 'Full Setup'; 
                           System.debug('AccountTriggerHelper:updateOnboardingAddress: Default Onboard Request type changed = ' + onboard.Request_Type__c);
                       }
                        updatedOnboardings.add(onboard);
 
                    }                       
                }
            }
        }
        // Checking if the list is not empty and updating the Onboarding Records
        if(!updatedOnboardings.isEmpty()){
            Database.update(updatedOnboardings, True);
    
       }
    }*/
}