/***************************************************
 * CreatedBy: Accenture
 * CreatedDate: 2/28/2020
 * @Desc: Creates a mock response which mirrors the response from the OAuthConnector class
 ***************************************************/
@isTest
global class OAuthConnectorMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"refresh_token_expires_in":"0","api_product_list":"[PHARMA-ORPHANA, PHARMA-ML, PHARMA-MDG, PHARMA-ECM, PHARMA-SAP]","api_product_list_json":["PHARMA-ORPHANA","PHARMA-ML","PHARMA-MDG","PHARMA-ECM","PHARMA-SAP"],"organization_name":"cahapi-nonprod","developer.email":"amarenderreddy.narsimhula@cardinalhealth.com","token_type":"Bearer","issued_at":"1582922771699","client_id":"TEWSDFTWJjeje","access_token":"qwertypoiuy","application_name":"xxyyyzzz","scope":"","expires_in":"17999","refresh_count":"0","status":"approved"}');
        res.setStatusCode(200);

        return res;
    }
}