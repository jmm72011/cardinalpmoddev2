/*************************************************************************************************************************************************************
@ Class:          ContinuationService
@ Version:        1.0
@ Author:         Vasantha Nelahonne
@ Purpose:        Class for making Continuation REST Callouts
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/

public with sharing virtual class ContinuationService {

    /*Constants - begin*/
    private static final String CLASS_NAME                         = 'ContinuationService';
    private static final String LOG_TYPE                           = 'ERROR';
    private static final String ALERT_TYPE                         = 'APPLICATION_ALERTS';
    private final static String ALERT_MESSAGE_EXE                  = 'WebService Exception';
    private final static String ALERT_MESSAGE_DML                  = 'DML Exception';
    private static final String SEVERITY_CRITICAL                  = 'CRITICAL';
    private static final String SEVERITY_HIGH                      = 'HIGH';
    private static final Integer CONTINUATION_CALL_LIMIT           = 3;

    /*Constants - end*/

    public ContinuationState calloutState;
    //public HTTPRequest        serviceRequest { get; set; }
    public HTTPResponse         serviceResponse{ get; set; }
    public Object               appRequest;
    public String               serviceName;
    public String               serviceId;


   /*********************************************************************************************************************************************************
    @ Constructor:    ContinuationService
    @ Version:        1.0
    @ Author:         Vasantha Nelahonne
    @ Purpose:        Initialises the class for controller extension instance.
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Change history:
    **********************************************************************************************************************************************************/
    public ContinuationService() {
        calloutState = new ContinuationState();
    }

    public ContinuationService(ApexPages.StandardController controller) {

        calloutState = new ContinuationState();

    }


    /*********************************************************************************************************************************************************
    @ Method:         continuationCallout
    @ Version:        1.0
    @ Author:         Vasantha Nelahonne
    @ Purpose:        Searches for the products, inventory and pricing all together and bundles the data together and sends the response
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        Continuation
    **********************************************************************************************************************************************************/
    public static Continuation continuationCallout( HTTPRequest calloutRequest, String callback, Object appRequest )
    {

        System.Debug('Entering: continuationCallout');

        //make continuation call
        try
        {
            //ContinuationService.ContinuationState calloutState =  new ContinuationService.ContinuationState( requestData);
            ContinuationState calloutState =  new ContinuationState( appRequest);
            Continuation cont = new Continuation(120);
            cont.continuationMethod = callback;
            //cont.continuationMethod = CALLBACKMETHOD;

            // Add callout request to continuation
            //calloutState.contService = (ContinuationService) serviceInstance;\
           
            calloutState.serviceId = cont.addHttpRequest(calloutRequest);
            system.debug('ContinuationService : continuationCallout serviceId : ' + calloutState.serviceId);
            cont.state = calloutState;
            System.Debug('ContinuationService : continuationCallout calloutState : ' + calloutState);
            System.Debug('ContinuationService : continuationCallout cont : ' + cont);
            //System.Debug('continuationCallout Callback will be handled in method : ' + cont.continuationMethod);
            System.Debug('Exiting ContinuationService: continuationCallout');
            system.debug('values of continutaion'+cont);
            return cont;
        }
        catch(Exception e)
        {
            System.Debug('Continuation Exception: Error occured in Continuation callout');
            System.Debug('Continuation Exception: ' + e.getMessage());
            System.Debug('Continuation Exception: ' + e.getStackTraceString());
            //return errorMessage;
            return null;

        }
   }

    public static Continuation continuationCallout( HTTPRequest calloutRequest, String callback, Object appRequest, Object serviceInstace )
    {

        System.Debug('Entering: continuationCallout');

        //make continuation call
        try
        {
            //ContinuationService.ContinuationState calloutState =  new ContinuationService.ContinuationState( requestData);
            ContinuationState calloutState =  new ContinuationState( appRequest);
            Continuation cont = new Continuation(120);
            cont.continuationMethod = callback;
            //cont.continuationMethod = CALLBACKMETHOD;

            // Add callout request to continuation
            //calloutState.contService = (ContinuationService) serviceInstance;
            
            calloutState.serviceId = cont.addHttpRequest(calloutRequest);
            cont.state = calloutState;
            System.Debug('ContinuationService : continuationCallout calloutState : ' + calloutState);
            System.Debug('ContinuationService : continuationCallout cont : ' + cont);
            //System.Debug('continuationCallout Callback will be handled in method : ' + cont.continuationMethod);
            System.Debug('Exiting ContinuationService: continuationCallout');
            return cont;
        }
        catch(Exception e)
        {
            System.Debug('Continuation Exception: Error occured in Continuation callout');
            System.Debug('Continuation Exception: ' + e.getMessage());
            System.Debug('Continuation Exception: ' + e.getStackTraceString());
            //return errorMessage;
            return null;

        }
   }

    public static Continuation parallelContinuationCallout( List<HTTPRequest> calloutRequests, List<String> serviceNames, String callback, Object appRequest )
    {

        System.Debug('Entering: continuationCallout');

        if(calloutRequests.size() > CONTINUATION_CALL_LIMIT)
        {
            System.Debug('groupContinuationCallout : Error : Number of Parallel Callouts exceeds Continuation Governor Limit ');
            return null;
        }

        try
        {
            ContinuationState calloutState =  new ContinuationState( appRequest, serviceNames);
            Continuation cont = new Continuation(120);
            cont.continuationMethod = callback;

            // Add parallel callout requests to continuation
            List<String> serviceIds = new List<String>();
            for(Integer i=0; i < calloutRequests.size(); i++ )
            {
                String serviceId = cont.addHttpRequest(calloutRequests[i]);
                serviceIds.add(serviceId);
            }

            calloutState.serviceIdList = serviceIds;
            cont.state = calloutState;
            System.Debug('ContinuationService : continuationCallout calloutState : ' + calloutState);
            System.Debug('ContinuationService : continuationCallout cont : ' + cont);
            //System.Debug('continuationCallout Callback will be handled in method : ' + cont.continuationMethod);
            System.Debug('Exiting ContinuationService: continuationCallout');
            return cont;
        }
        catch(Exception e)
        {
            System.Debug('Continuation Exception: Error occured in Continuation callout');
            System.Debug('Continuation Exception: ' + e.getMessage());
            System.Debug('Continuation Exception: ' + e.getStackTraceString());
            //return errorMessage;
            return null;

        }
   }


    public  ContinuationService continuationServiceResponse(Object state)
    {
        System.Debug('TIMESTAMP:Entering ContinuationService : continuationCallback ' );
        System.Debug('ContinuationService : continuationCallback: ' + state);
        try{
            ContinuationService cs = new ContinuationService();
        ContinuationState  callState = (ContinuationState ) state;
        //Retreive the response  back
        System.Debug('service id : ' + callState.serviceId);
        System.Debug('service id : ' + callState.starttime);
        System.Debug('ContinuationService : continuationServiceResponse: state.serviceId ' + callState.serviceId);
        HttpResponse hresp = (HttpResponse) Continuation.getResponse(callState.serviceId);
        System.Debug('continuationCallback : serviceResponse ' + hresp);
        cs.calloutState = (ContinuationState) state;
        cs.serviceResponse = hresp;

        return cs;
        }catch(Exception e){
            System.Debug('Continuation Exception: Error occured in Continuation callout');
            System.Debug('Continuation Exception: ' + e.getMessage());
            System.Debug('Continuation Exception: ' + e.getStackTraceString());

            return null;
        }
        

    }

    public  List<ContinuationService> continuationServiceResponseList(Object state)
    {
        System.Debug('TIMESTAMP:Entering continuationServiceResponseList' );
        try{
            ContinuationState  calloutState = (ContinuationState ) state;
        System.Debug('continuationServiceResponseList : ' + calloutState);
        List<ContinuationService> responsesList = new List<ContinuationService>();
        List<String> serviceIdList = calloutState.serviceIdList;
        ContinuationService cs;
        for ( Integer i=0; i < serviceIdList.size(); i++)
        {
            System.Debug('service id : ' + serviceIdList[i]);

            cs = new ContinuationService();
            cs.calloutState = (ContinuationState ) state;
            cs.serviceId = serviceIdList[i];
            cs.serviceName = cs.calloutState.serviceNameList[i];
            cs.appRequest = cs.calloutState.requestData;

            HttpResponse hresp = (HttpResponse) Continuation.getResponse(cs.serviceId);
            cs.serviceResponse = hresp;
            System.Debug('continuationCallback : serviceResponse for service : ' + cs.serviceName + ' : '  + cs);

            responsesList.add(cs);
        }
        System.Debug('continuationServiceResponseList: Returning parallel responses List : ' + responsesList);
        return responsesList;
        }catch(Exception e){
            System.Debug('Continuation Exception: Error occured in Continuation callout');
            System.Debug('Continuation Exception: ' + e.getMessage());
            System.Debug('Continuation Exception: ' + e.getStackTraceString());

            return null;
        }
        

    }
    
    

}