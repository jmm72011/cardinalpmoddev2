/*************************************************************************************************************************************************************
    @ Class:          SendToMdgService
    @ Version:        1.0
    @ Author:         RANJAN SAMAL (ranjan.samal@accenture.com)
    @ Purpose:        Helper class for invoking sending to MDG connector class from onclick javscript from "Submit To MDG" button.
    --------------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Change history: 18.08.2015 / RANJAN SAMAL / Created the class.
    **************************************************************************************************************************************************************/
    
    global with sharing class SendToMdgService {
            
        /*********************************************************************************************************************************************************
        @ Method:         sendToMDG
        @ Version:        1.0
        @ Author:         RANJAN SAMAL (ranjan.samal@accenture.com)
        @ Purpose:        Method to invoke connectorMethod in SAPMDGConnector 
        ----------------------------------------------------------------------------------------------------------------------------------------------------------
        @ Parameters:     onbId - Passed to fetch the particular Onboarding record Status based on the Id
        ----------------------------------------------------------------------------------------------------------------------------------------------------------
        @ Returns:        String - Returns the failure message to show on the screen while submitting the record to MDG
        ----------------------------------------------------------------------------------------------------------------------------------------------------------
        @ Change history: 18.08.2015 / RANJAN SAMAL / Created the method.
            ******************************************************************************************************************************************************/
        webservice static String sendToMDG(String OnbId) { 
            
            /*CONSTANTS - begin --*/
            final  String SUCCESS                   = '0';
            final  String REJECTED                  = 'E';
            final  String CALLOUT_ERROR             = 'X';
            final  String INITIAL_SETUP             = 'Initial Setup';
            final  String FULL_SETUP                = 'Full Setup';
            final  String INITIAL_TO_FULL_SETUP     = 'Initial to Full Setup';
            final  String CLASS_NAME                = 'SendToMdgService';        /*Stores Class Name */
            final String METHOD_NAME          = 'sendToMDG';
            final String LOG_TYPE             = 'ERROR';
            final String ALERT_TYPE           = 'APPLICATION_ALERTS';
            final String  ALERT_MESSAGE_EXE           = 'WebService Exception';  
            final String  ALERT_MESSAGE_DML           = 'DML Exception';   
            final String SEVERITY_CRITICAL    = 'CRITICAL';
            final String SEVERITY_HIGH        = 'HIGH';
            
            
            /*CONSTANTS - end --*/
            String szStatus;            /*final return status string*/
            
            
            /**Initializing the Response Class*/
            try{           
                  SAPMDG_WebService.BAPIRET2_T response = SAPMDGConnector.connectorMethod(OnbId);
                
            /**Fetching the status from onBoarding based on the onboarding ID*/
            Onboarding__c onbd = [SELECT Id,
                                         Onboarding_Status__c,
                                         Request_Type__c 
                                   FROM  Onboarding__c  
                                   WHERE Id =: OnbId];
            system.debug('---------->>>>>' + response);       
            /*Null Check*/
            //try{
                if(response != NULL )
                {
                    if(response.item != NULL && response.item.size() > 0)
                    {
                             /*If In the Response we recieve in Item --Type field as 0,then onboarding was submitted to MDG successfully*/
                            system.debug('---------->>>>>hi1' + response.item[0].Type_x == SUCCESS && onbd.Request_Type__c == 'Full Setup' );       
                            system.debug('---------->>>>>hi2' + response.item[0].Type_x);       
                            system.debug('---------->>>>>hi3' + response.item[0].MESSAGE);       
                            system.debug('---------->>>>>hi4' + response.item[0].MESSAGE_V4);       
                            system.debug('---------->>>>>hi5' + onbd.Request_Type__c);
                            system.debug('---------->>>>>ONB' + onbd);       
                            
                            if(response.item[0].Type_x == SUCCESS && onbd.Request_Type__c == 'Initial Setup'){
                                onbd.Onboarding_Status__c  = System.Label.SubmitStatus;                    
                                onbd.Previous_Request_Type__c =  response.item[0].MESSAGE_V4;
                                Database.Update(onbd) ;
                                szStatus = System.Label.Onboarding_Success_Response;  
                                
                            
                            } else if(response.item[0].Type_x == SUCCESS && onbd.Request_Type__c == 'Full Setup' 
                                                            || (response.item[0].Type_x == SUCCESS && onbd.Request_Type__c == 'Initial to Full Setup')) {
                                onbd.Onboarding_Status__c  = System.Label.SubmitStatus;
                                onbd.Previous_Request_Type__c =  response.item[0].MESSAGE_V4;
                                Database.Update(onbd) ;
                                szStatus = System.Label.OnboardingSuccessforFullOnboarding;  
                             } 
                           else if(response.item[0].Type_x == REJECTED) {            
                               szStatus = System.Label.SendToMDGRejectedErrorMsg;     
                             }
                            else if(response.item[0].Type_x == CALLOUT_ERROR) {            
                               szStatus = response.item[0].MESSAGE_V4;  
                             }
                           else {
                                 szStatus = 'Onboarding request sent from Salesforce.   Unidentified Error Response from MDG';  //add a system.label for this message
                                 //szStatus = System.Label.SendToMDGErrorMsg; 
                                 ApexLogger.sendAlert(szStatus, CLASS_NAME, METHOD_NAME, 'ERROR', 'INTERFACE_ALERTS', 'HIGH' );
                            }
                    
                    }
                    else
                    {
                         szStatus = System.Label.SendToMDGLockedErrorMsg; 
                    }
           } 
           else {
     
        
                szStatus = System.Label.SendToMDGErrorMsg;
                 /* -- Send alert to represented subscribers when callOut gets any DMLerror ---*/
                 ApexLogger.sendAlert(ALERT_MESSAGE_DML, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_HIGH );
    
            }}
            catch (exception exp) {   
                      szStatus = System.Label.SendToMDGErrorMsg;                               
                       /* -- Send alert to represented subscribers when callOut gets any Exception ---*/
                      ApexLogger.sendAlert(ALERT_MESSAGE_EXE, CLASS_NAME, METHOD_NAME, LOG_TYPE, ALERT_TYPE, SEVERITY_CRITICAL );
                      return szStatus;
               }
            
            return szStatus;
        }
    }