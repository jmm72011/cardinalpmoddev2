/*************************************************************************************************************************************************************
@ Class:          RESTServiceData
@ Version:        1.0
@ Author:         Vasantha Nelahonne
@ Purpose:        Class for making  REST Data
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/

public with sharing abstract class RESTServiceData
{

    //1. Required Method
    //Required for both POST Callout and GET Callout
    //All Service Data extended class must implement following method
    //public  abstract Object retrieveJSONResponse(HTTPResponse jsonResponse);
    public  abstract Object retrieveJSONResponse(Object response);

    //2. Optional Method
    //Required only for POST Callout.  NOT REQUIRED for GET Callout
    //public virtual JSONGenerator generateJSONRequest(Object request)
    public virtual Object generateJSONRequest(Object request){   
        System.Debug('generateJSONRequest : inside virtual');
        return null;}
    
    public virtual Object createServiceRequestData(Object appRequest, String calloutMethod)
    {
        System.Debug('generateJSONRequest : inside virtual');
        return null;
    }
    

}