/*************************************************************************************************************************************************************
@ Class:          COGSService
@ Version:        1.0
@ Author:         Bibekini Behera
@ Purpose:        Service class for getting COGS Data.
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history:
**************************************************************************************************************************************************************/
global with sharing class COGSService{


    //Every Service must have following mandatory fields set
    final public static String SERVICE_ENDPOINT = 'PH_SAP_SOH_CogsService';
    final public static String SERVICE_CALLBACK = 'getCOGSCallback';

 	/*********************************************************************************************************************************************************
	@ Constructor:    COGSService
	@ Version:        1.0
	@ Author:         Bibekini Behera
	@ Purpose:        Initialises the class for controller extension instance.
	----------------------------------------------------------------------------------------------------------------------------------------------------------
	@ Change history:
	**********************************************************************************************************************************************************/
    public COGSService(ApexPages.StandardController controller) { }


  /*********************************************************************************************************************************************************
    @ Method:         getCOGS
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        App Service for getting COGSData Data
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        Continuation
    **********************************************************************************************************************************************************/
    @RemoteAction
    global static Object getCOGS(CustomerInfo custInfo)
    {
        System.Debug('Entering COGSService: getCOGSData: ' + custInfo);

        Endpoints__c  serviceEndPoint = RESTService.getEndPoint(SERVICE_ENDPOINT);
        if( serviceEndPoint != null && serviceEndPoint.Is_Call_Active__c)
        {
            return COGSServiceREST.serviceCallout(custInfo, serviceEndPoint, SERVICE_CALLBACK );
        }
        else
        {
            System.Debug('EndPoint : ' + SERVICE_ENDPOINT + ' : Error --- Either EndPoint is missing OR EndPoint NOT Configured for Call Active.  No Callout made');
            ServiceMessage errorMessage = ServiceMessage.sendErrorMessage( 'Call is NOT Active: NO Callout is made.  Returning empty null data' , '', 'getCOGS' );
            return errorMessage; 
        }
        //return null;
    }

	/*********************************************************************************************************************************************************
    @ Method:         getCOGSCallback
    @ Version:        1.0
    @ Author:         Bibekini Behera
    @ Purpose:        CallBack method
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Parameters:
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Returns:        Response
    **********************************************************************************************************************************************************/
   public static Object getCOGSCallback(Object state)
    {
        try
        {
            System.Debug('Entering getCOGSDataCallback ' );
            COGSServiceREST restService = new COGSServiceREST();
            //CustomerInfo restResponse = (CustomerInfo) restService.serviceResponse(state);
            //changed as part of Error Handling 
            Object obj = (Object) restService.serviceResponse(state);
            if(obj instanceof ServiceMessage){
                return obj;
            }else{
               CustomerInfo restResponse = (CustomerInfo) obj;
               System.Debug('getCOGSDataCallback Returning Data to App UI : ' + restResponse);
               return restResponse;
            }
            
        }
        catch(Exception e)
        {
            System.Debug('Exception: getCOGSDataCallback ' + e.getMessage() );
            System.Debug('Exception: getCOGSDataCallback ' + e.getStackTraceString() );
            ServiceMessage errorMessage = ServiceMessage.sendErrorMessage( 'get Cogs Callback Exception : ' + e.getMessage(), '', 'Retrieve cogs data' );
            return errorMessage;
            
        }
        
    }
}