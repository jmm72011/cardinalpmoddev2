/*****************************************************************************************************************************
@ Class:        CheckRecursive
@ Version:      1.0
@ Author:       Mohan Swarna (mohan.swarna@accenture.com)
@ Purpose:      Class for Avoiding Recursive trigger .
--------------------------------------------------------------------------------------------------------------------------------
@ Change history: 10.30.2015 / MOHAN SWARNA / Created the Class.
********************************************************************************************************************************/
public Class CheckRecursive{
    public static boolean run = true;
    /*****************************************************************************************************************************
    @ Class    :        runOnce
    @ Version  :        1.0
    @ Author   :        Mohan Swarna (mohan.swarna@accenture.com)
    @ Purpose  :        Method for run recursive.
    -----------------------------------------------------------------------------------------------------------------------------
    @ Change history: 10.30.2015 / MOHAN SWARNA / Created the Method.
    ****************************************************************************************************************************/
    public static boolean runOnce(){
        if(run){
             run=false;
             return true;
        }else{
            return run;
        }
    }
}