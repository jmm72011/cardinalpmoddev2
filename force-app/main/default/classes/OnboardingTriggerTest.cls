/*************************************************************************************************************************************************************
@ Class:          OnboardingTriggerTest
@ Version:        1.0
@ Author:         Priyanka Dwivedi (p.dwivedi@accenture.com)
@ Purpose:        Test class for Attachmenttrigger
--------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history: 11.27.2015 / Priyanka Dwivedi / Created the class.
**************************************************************************************************************************************************************/

@isTest(seeAllData = false)
private class OnboardingTriggerTest{

    /*********************************************************************************************************************************************************
    @ Method:         testOnboardingTrigger
    @ Version:        1.0
    @ Author:         Priyanka Dwivedi (p.dwivedi@accenture.com)
    @ Purpose:        Test method for Attachment Trigger                       
                                                         
                      COVERAGES     PMod_OnboardingTrigger
                      =========     |
                                    *--> PMod_OnboardingTriggerHelper.callOnbdTrigger(Set<Id> onboardingIds)                                    
    ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @ Change history: 10.11.2015 / Priyanka Dwivedi / Created the  test Method.
    **********************************************************************************************************************************************************/
    static testMethod void testOnboardingTrigger() {
        /*TEST DATA ---------------------------------------------------------------------------------------------------------------------- BEGIN ------------*/
        Map<String, List<Sobject>> testDataMap = TestDataFactory.setUpOnboardingTriggerData();    /*Calling TestDataFactory Method */
        Map<String, List<Sobject>> testDataMap1 = TestDataFactory.setUpPModSAPMDGConnectorData();
        List<Account> acc = ((List<Account>)(testDataMap.get('accounts')));
        List<Onboarding__c> onboardings = ((List<Onboarding__c>)(testDataMap.get('Onboarding')));
        List<Cardinal_Business_Unit__c> cardinalBURecs      = ((List<Cardinal_Business_Unit__c>)(testDataMap1.get('cardinalBuDatas'))); //Added by Shreyashi
        cardinalBURecs[0].Active__c = 'Yes';
        //cardinalBURecs[1].Active__c = 'Yes';
        update cardinalBURecs;
        User adminUsr = TestDataFactory.createUser('System Administrator');
        User stdUsr = TestDataFactory.createUser('Standard User');        
        /*TEST DATA ---------------------------------------------------------------------------------------------------------------------- END --------------*/ 
        
        /*PROFILE TESTING ---------------------------------------------------------------------------------------------------------------- BEGIN ------------*/
        //Profile 1: Sytem Administrtor
        System.runAs(adminUsr) {
            Test.startTest(); 
                
                /*NEGATIVE TESTING ------------------------------------------------------------------------------------------------------- BEGIN ------------*/
                onboardings[0].CSOS__c = false;
                onboardings[0].Approved__c = true;
                update onboardings;
                System.assertEquals(onboardings[0].Approved__c,true);
                /*NEGATIVE TESTING ------------------------------------------------------------------------------------------------------- END --------------*/ 
                
                /*POSITIVE TESTING ------------------------------------------------------------------------------------------------------- BEGIN ------------*/
                onboardings[0].Approved__c = true;
                onboardings[0].Sync_Fields__c = true;
                onboardings[0].Onboarding_Status__c='Submit Pending';
                onboardings[0].Cardinal_Business_Unit__c = cardinalBURecs[0].Id;
                update onboardings;
                System.assertEquals(onboardings[0].Sync_Fields__c,true);
                System.assertEquals(onboardings[0].Approved__c,true);
                System.assertEquals(onboardings[0].Name, 'Test Onbaord0');
                System.assertEquals(onboardings[0].EDI_Setup__c, true);
               // System.assertEquals(onboardings[0].Business_Unit__c, 'Parmed');
                /*POSITIVE TESTING ------------------------------------------------------------------------------------------------------- END --------------*/  
                
                /*BULK TESTING ----------------------------------------------------------------------------------------------------------- BEGIN ------------*/ 
                List<Account> accs = TestDataFactory.createAccounts(4, 'CAH Prospect'); 
                List<Onboarding__c> onboards = TestDataFactory.createOnboardings(20,((List<Account>)(accs))[0].Id);
                System.assertEquals(onboards[3].Name, 'Test Onbaord3');
                //System.assertEquals(onboards[4].Business_Unit__c, 'Parmed');              
                /*BULK TESTING ----------------------------------------------------------------------------------------------------------- END --------------*/
            Test.stopTest();                        
        }
    }
	static testmethod void testOnboardingMDGCall()
    {
        ActiveTriggers__c acTrigger=new ActiveTriggers__c();
        acTrigger.AccountTrigger__c=true;
        acTrigger.Name='P-MOD TRIGGER CONTROL';
        insert acTrigger;
        
        List<Account> accounts = TestDataFactory.createAccounts(1, 'CAH Customer');
        List<Onboarding__c> Onboardings = TestDataFactory.createOnboardings(1,((List<Account>)(accounts))[0].Id);
        String onbRecordtypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('SoldTo').getRecordTypeId();
        Onboardings[0].RecordtypeId = onbRecordtypeId;
        Onboardings[0].Onboarding_Status__c = 'Submit Pending';
        update Onboardings;
    }    

    static testmethod void testMaintenanceMDGCall()
    {
        ActiveTriggers__c acTrigger=new ActiveTriggers__c();
        acTrigger.AccountTrigger__c=true;
        acTrigger.Name='P-MOD TRIGGER CONTROL';
        insert acTrigger;
        
        List<Account> accounts = TestDataFactory.createAccounts(1, 'CAH Customer');
        List<Onboarding__c> maintenaceRecs = TestDataFactory.createOnboardings(1,((List<Account>)(accounts))[0].Id);
        String maintRecTypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId();
        maintenaceRecs[0].RecordtypeId = maintRecTypeId;
        maintenaceRecs[0].Onboarding_Status__c = 'Submit Pending';
        maintenaceRecs[0].Customer_Account_Group__c = 'ZPST';
        maintenaceRecs[0].Account_Request_Type__c = '002; 004; 006';
        maintenaceRecs[0].Business_Unit__c = '';
        Test.startTest();
            update maintenaceRecs;
            
            

            Onboarding__c maintRecUpdated = [SELECT Id, Onboarding_Status__c, Account_Request_Type__c FROM Onboarding__c WHERE Id = :maintenaceRecs[0].Id];
            system.assertEquals('002', maintRecUpdated.Account_Request_Type__c);
            maintRecUpdated.Onboarding_Status__c = 'Cancelled by System';

            update maintRecUpdated;
        Test.stopTest();
    } 

    static testmethod void testMaintenanceSplit()
    {
        ActiveTriggers__c acTrigger=new ActiveTriggers__c();
        acTrigger.AccountTrigger__c=true;
        acTrigger.Name='P-MOD TRIGGER CONTROL';
        insert acTrigger;
        
        List<Account> accounts = TestDataFactory.createAccounts(1, 'CAH Customer');
        List<Onboarding__c> maintenaceRecs = TestDataFactory.createOnboardings(2,((List<Account>)(accounts))[0].Id);
        String maintRecTypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('Maintenance').getRecordTypeId();
        maintenaceRecs[0].RecordtypeId = maintRecTypeId;
        maintenaceRecs[0].Onboarding_Status__c = 'Submit Pending';
        maintenaceRecs[0].Customer_Account_Group__c = 'ZPST';
        maintenaceRecs[0].Account_Request_Type__c = '001; 003; 005';
        maintenaceRecs[0].Business_Unit__c = '';
        maintenaceRecs[1].RecordtypeId = maintRecTypeId;
        maintenaceRecs[1].Onboarding_Status__c = 'Submit Pending';
        maintenaceRecs[1].Customer_Account_Group__c = 'ZPST';
        maintenaceRecs[1].Account_Request_Type__c = '013; 010';
        maintenaceRecs[1].Business_Unit__c = '';

        Set<Id> maintRecIds = new Set<Id>{maintenaceRecs[0].id, maintenaceRecs[1].id};
        Test.startTest();
            update maintenaceRecs;
            
            

            List<Onboarding__c> maintRecUpdated = [SELECT Id, Onboarding_Status__c, Account_Request_Type__c FROM Onboarding__c WHERE Id IN : maintRecIds];
            system.assert(maintRecUpdated[0].Account_Request_Type__c == '001' || maintRecUpdated[1].Account_Request_Type__c.split(';').contains('013'));
            system.debug(maintRecUpdated[1].Account_Request_Type__c);
            system.assert(maintRecUpdated[1].Account_Request_Type__c == '001' || maintRecUpdated[1].Account_Request_Type__c.split(';').contains('013'));



        Test.stopTest();
    } 

    static testMethod void testUpdtChkBoxes(){
        ActiveTriggers__c acTrigger=new ActiveTriggers__c();
        acTrigger.AccountTrigger__c=true;
        acTrigger.Name='P-MOD TRIGGER CONTROL';
        insert acTrigger;

        List<Onboarding__c> onbToIns = new List<Onboarding__c>();

        Id soldToRecTypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('SoldTo').getRecordTypeId();
        Id shipToRecTypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('BillTo').getRecordTypeId();
        Id billToRecTypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('ShipTo').getRecordTypeId();
        Id payerRecTypeId = Schema.Sobjecttype.Onboarding__c.getRecordTypeInfosByName().get('Payer').getRecordTypeId();

        Onboarding__c testSoldTo = new Onboarding__c(Name = 'Test solto',
                                            Account_Name__c = 'Test solto',
                                            Act_as_Bill_to__c = true,
                                            Act_as_Ship_to__c = true,
                                            Act_as_Payer__c = true,
                                            Servicing_Group__c = 'NATIONAL ACCOUNT',
                                            Payment_Type__c = 'ACH',
                                            Payment_Terms__c = 'Weekly Credit Card',
                                            Bill_To_Country__c = 'USA',
                                            Ship_To_Country__c = 'USA',
                                            Bill_To_Invoice_Method__c = 'Invoice - Print',
                                            Business_Unit__c ='PD',
                                            EDI_Setup__c = true,
                                            Sticker_Perference_del__c='Price Sticker with Pricing',
                                            KYC__c='No',
                                            Has_Completed_the_Credit_Application__c= true,
                                            Initially_Purchase_Controlled_Substance__c= true,
                                            Rebate_Preferences__c='Credit',
                                            Lot_Expiration_Preferences__c='30',
                                            CSOS__c= true,
                                            Approved__c = false,
                                            Priority__c='Normal',
                                            Bill_To_License_Name__c='tesrty',
                                            Bill_To_Telephone__c='123654789',
                                            Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
                                            Ship_To_Email_Address__c='test@tset.com',
                                            Ship_To_Fax_Number__c='515225-3170',
                                            Ship_To_Phone_Number__c='515225-3170',
                                            Bill_To_Zip_Code__c='55440',
                                            Bill_To_City__c='MINNEAPOLIS',
                                            Bill_To_State__c='MN',
                                            Bill_To_Address1__c='PO BOX 9493',
                                            Ship_To_Zip_Code__c='45678',
                                            Ship_To_City__c='city',
                                            Ship_To_State__c='IA',
                                            Request_Type__c='Full Setup',
                                            Sync_Fields__c = false,
                                            Credit_Limit__c=234.00,
                                            Customer_Classification__c='sss',
                                            Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
                                            Estimated_First_Purchase_Date__c = system.today(),
                                            RecordTypeId = soldToRecTypeId,
                                            Is_this_new_business_to_Cardinal_Health__c ='Yes,',
                                            Requested_Credit_Limit__c =123.00);
        
        insert testSoldTo;

        Onboarding__c testShipTo = new Onboarding__c(Name = 'Test Onboard',
                                            Account_Name__c = 'Test shipto',
                                            Parent_Id__c = testSoldTo.Id,
                                            Servicing_Group__c = 'NATIONAL ACCOUNT',
                                            Payment_Type__c = 'ACH',
                                            Payment_Terms__c = 'Weekly Credit Card',
                                            Bill_To_Country__c = 'USA',
                                            Ship_To_Country__c = 'USA',
                                            Bill_To_Invoice_Method__c = 'Invoice - Print',
                                            Business_Unit__c ='PD',
                                            EDI_Setup__c = true,
                                            Sticker_Perference_del__c='Price Sticker with Pricing',
                                            KYC__c='No',
                                            Has_Completed_the_Credit_Application__c= true,
                                            Initially_Purchase_Controlled_Substance__c= true,
                                            Rebate_Preferences__c='Credit',
                                            Lot_Expiration_Preferences__c='30',
                                            CSOS__c= true,
                                            Approved__c = false,
                                            Priority__c='Normal',
                                            Bill_To_License_Name__c='tesrty',
                                            Bill_To_Telephone__c='123654789',
                                            Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
                                            Ship_To_Email_Address__c='test@tset.com',
                                            Ship_To_Fax_Number__c='515225-3170',
                                            Ship_To_Phone_Number__c='515225-3170',
                                            Bill_To_Zip_Code__c='55440',
                                            Bill_To_City__c='MINNEAPOLIS',
                                            Bill_To_State__c='MN',
                                            Bill_To_Address1__c='PO BOX 9493',
                                            Ship_To_Zip_Code__c='45678',
                                            Ship_To_City__c='city',
                                            Ship_To_State__c='IA',
                                            Request_Type__c='Full Setup',
                                            Sync_Fields__c = false,
                                            Credit_Limit__c=234.00,
                                            Customer_Classification__c='sss',
                                            Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
                                            Estimated_First_Purchase_Date__c = system.today(),
                                            RecordTypeId = shipToRecTypeId,
                                            Is_this_new_business_to_Cardinal_Health__c ='Yes,',
                                            Requested_Credit_Limit__c =123.00);

        onbToIns.add(testShipTo);

        Onboarding__c testBillTo = new Onboarding__c(Name = 'Test Onboard',
                                            Account_Name__c = 'Test billto',
                                            Parent_Id__c = testSoldTo.Id,
                                            Servicing_Group__c = 'NATIONAL ACCOUNT',
                                            Payment_Type__c = 'ACH',
                                            Payment_Terms__c = 'Weekly Credit Card',
                                            Bill_To_Country__c = 'USA',
                                            Ship_To_Country__c = 'USA',
                                            Bill_To_Invoice_Method__c = 'Invoice - Print',
                                            Business_Unit__c ='PD',
                                            EDI_Setup__c = true,
                                            Sticker_Perference_del__c='Price Sticker with Pricing',
                                            KYC__c='No',
                                            Has_Completed_the_Credit_Application__c= true,
                                            Initially_Purchase_Controlled_Substance__c= true,
                                            Rebate_Preferences__c='Credit',
                                            Lot_Expiration_Preferences__c='30',
                                            CSOS__c= true,
                                            Approved__c = false,
                                            Priority__c='Normal',
                                            Bill_To_License_Name__c='tesrty',
                                            Bill_To_Telephone__c='123654789',
                                            Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
                                            Ship_To_Email_Address__c='test@tset.com',
                                            Ship_To_Fax_Number__c='515225-3170',
                                            Ship_To_Phone_Number__c='515225-3170',
                                            Bill_To_Zip_Code__c='55440',
                                            Bill_To_City__c='MINNEAPOLIS',
                                            Bill_To_State__c='MN',
                                            Bill_To_Address1__c='PO BOX 9493',
                                            Ship_To_Zip_Code__c='45678',
                                            Ship_To_City__c='city',
                                            Ship_To_State__c='IA',
                                            Request_Type__c='Full Setup',
                                            Sync_Fields__c = false,
                                            Credit_Limit__c=234.00,
                                            Customer_Classification__c='sss',
                                            Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
                                            Estimated_First_Purchase_Date__c = system.today(),
                                            RecordTypeId = billToRecTypeId,
                                            Is_this_new_business_to_Cardinal_Health__c ='Yes,',
                                            Requested_Credit_Limit__c =123.00);

        onbToIns.add(testBillTo);
        Onboarding__c testPayer = new Onboarding__c(Name = 'Test Onboard',
                                            Account_Name__c = 'Test payer',
                                            Parent_Id__c = testSoldTo.Id,
                                            Servicing_Group__c = 'NATIONAL ACCOUNT',
                                            Payment_Type__c = 'ACH',
                                            Payment_Terms__c = 'Weekly Credit Card',
                                            Bill_To_Country__c = 'USA',
                                            Ship_To_Country__c = 'USA',
                                            Bill_To_Invoice_Method__c = 'Invoice - Print',
                                            Business_Unit__c ='PD',
                                            EDI_Setup__c = true,
                                            Sticker_Perference_del__c='Price Sticker with Pricing',
                                            KYC__c='No',
                                            Has_Completed_the_Credit_Application__c= true,
                                            Initially_Purchase_Controlled_Substance__c= true,
                                            Rebate_Preferences__c='Credit',
                                            Lot_Expiration_Preferences__c='30',
                                            CSOS__c= true,
                                            Approved__c = false,
                                            Priority__c='Normal',
                                            Bill_To_License_Name__c='tesrty',
                                            Bill_To_Telephone__c='123654789',
                                            Ship_To_License_Name__c='TARGET PHARMACY/STORE T-0069',
                                            Ship_To_Email_Address__c='test@tset.com',
                                            Ship_To_Fax_Number__c='515225-3170',
                                            Ship_To_Phone_Number__c='515225-3170',
                                            Bill_To_Zip_Code__c='55440',
                                            Bill_To_City__c='MINNEAPOLIS',
                                            Bill_To_State__c='MN',
                                            Bill_To_Address1__c='PO BOX 9493',
                                            Ship_To_Zip_Code__c='45678',
                                            Ship_To_City__c='city',
                                            Ship_To_State__c='IA',
                                            Request_Type__c='Full Setup',
                                            Sync_Fields__c = false,
                                            Credit_Limit__c=234.00,
                                            Customer_Classification__c='sss',
                                            Ship_To_Address_1__c='1800 VALLEY WEST DRIVE"',
                                            Estimated_First_Purchase_Date__c = system.today(),
                                            RecordTypeId = payerRecTypeId,
                                            Is_this_new_business_to_Cardinal_Health__c ='Yes,',
                                            Requested_Credit_Limit__c =123.00);

        onbToIns.add(testPayer);

        Test.startTest();
            insert onbToIns;

            Onboarding__c updParent = [SELECT Act_as_Bill_To__c, Act_as_Ship_to__c, Act_as_Payer__c FROM Onboarding__c WHERE Id = : testSoldTo.Id];

            System.assertEquals(updParent.Act_as_Bill_To__c, false);
        Test.stopTest();

    }

}